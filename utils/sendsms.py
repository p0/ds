#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import atom
import gdata.calendar
import gdata.calendar.service
import random
import time
 
def sendSms(email, password, title, content='', where=''):
    cs = gdata.calendar.service.CalendarService()
    cs.email = email
    cs.password = password
    cs.source = 'Google-Calendar-SMS-5.0_' + str(random.randint(0, 10000))
    cs.ProgrammaticLogin()
    event = gdata.calendar.CalendarEventEntry()
    event.title = atom.Title(text=title)
    event.content = atom.Content(text=content)
    event.where.append(gdata.calendar.Where(value_string=where))
    start_time = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', time.gmtime(time.time() + 2 * 60))
    when = gdata.calendar.When(start_time=start_time, end_time=start_time)
    reminder = gdata.calendar.Reminder(minutes=1, extension_attributes={'method': 'sms'})
    when.reminder.append(reminder)
    event.when.append(when)
    new_event = cs.InsertEvent(event, '/calendar/feeds/default/private/full')
 
 
if __name__ == '__main__':

    def main():
        if len(sys.argv) < 6:
            print 'Incorrect arguments number. Usage: %s email password title content where' % sys.argv[0]
        sendSms(*sys.argv[1:])

    main()