Www::Application.routes.draw do

    

    get "home/index"

    resources :signin_or_signups


    resources :shipping_infos
    resources :make_a_choices


    get "sessions/new"
    get "cart/new"
    
    resources :sessions, only: [:new, :create, :destroy]

    resources :product_categories do
        resources :products
    end
    resources :users do
        resources :carts
    end
    resources :users do
        resources :shipping_infos
    end
    resources :carts

    
    resources :product_providers

    get '/main',                  :to => 'product_categories#index'
    get '/signin',                :to => 'sessions#new'
    get '/signout',               :to => 'sessions#destroy'
    get '/signup',                :to => 'users#new'
    post '/add_product/:id',       :to => 'carts#new'
    post '/delete_product/:id',    :to => 'carts#destroy'
    get '/signup_step2',          :to => 'shipping_infos#new'
    get '/cart',                  :to => 'carts#show'
    get '/make_a_choice',         :to => 'make_a_choices#index'
    get '/update_qnt',            :to => 'carts#update' 

    root :to => "home#index"

    # The priority is based upon order of creation:
    # first created -> highest priority.

    # Sample of regular route:
    #   match 'products/:id' => 'catalog#view'
    # Keep in mind you can assign values other than :controller and :action

    # Sample of named route:
    #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
    # This route can be invoked with purchase_url(:id => product.id)

    # Sample resource route (maps HTTP verbs to controller actions automatically):
    #   resources :products

    # Sample resource route with options:
    #   resources :products do
    #     member do
    #       get 'short'
    #       post 'toggle'
    #     end
    #
    #     collection do
    #       get 'sold'
    #     end
    #   end

    # Sample resource route with sub-resources:
    #   resources :products do
    #     resources :comments, :sales
    #     resource :seller
    #   end

    # Sample resource route with more complex sub-resources
    #   resources :products do
    #     resources :comments
    #     resources :sales do
    #       get 'recent', :on => :collection
    #     end
    #   end

    # Sample resource route within a namespace:
    #   namespace :admin do
    #     # Directs /admin/products/* to Admin::ProductsController
    #     # (app/controllers/admin/products_controller.rb)
    #     resources :products
    #   end

    # You can have the root of your site routed with "root"
    # just remember to delete public/index.html.
    # root :to => 'welcome#index'

    # See how all your routes lay out with "rake routes"

    # This is a legacy wild controller route that's not recommended for RESTful applications.
    # Note: This route will make all actions in every controller accessible via GET requests.
    # match ':controller(/:action(/:id))(.:format)'
end
