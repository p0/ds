--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: country; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE country AS ENUM (
    'RU',
    'US',
    'YA',
    'DE'
);


--
-- Name: currency; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE currency AS ENUM (
    'mdl',
    'kgs',
    'inr',
    'jpy',
    'uah',
    'brl',
    'sek',
    'sgd',
    'uzs',
    'usd',
    'tmt',
    'aud',
    'chf',
    'zar',
    'byr',
    'cny',
    'eur',
    'huf',
    'kzt',
    'nok',
    'amd',
    'tjs',
    'xdr',
    'pln',
    'azn',
    'czk',
    'krw',
    'bgn',
    'try',
    'gbp',
    'lvl',
    'ron',
    'cad',
    'dkk',
    'ltl'
);


--
-- Name: user_role; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE user_role AS ENUM (
    'admin',
    'client'
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: assemble_accounts; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE assemble_accounts (
    id integer NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL,
    name character varying,
    surname character varying,
    dob date,
    phone character varying,
    gender boolean,
    address character varying,
    city character varying,
    state character varying,
    country country,
    postcode character varying,
    product_provider_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: assemble_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE assemble_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: assemble_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE assemble_accounts_id_seq OWNED BY assemble_accounts.id;


--
-- Name: cart_products; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE cart_products (
    id integer NOT NULL,
    cart_id integer NOT NULL,
    product_id integer NOT NULL,
    quantity integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: cart_products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cart_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cart_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cart_products_id_seq OWNED BY cart_products.id;


--
-- Name: carts; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE carts (
    id integer NOT NULL,
    user_id integer,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: carts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE carts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: carts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE carts_id_seq OWNED BY carts.id;


--
-- Name: currency_courses; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE currency_courses (
    id integer NOT NULL,
    aud numeric NOT NULL,
    azn numeric NOT NULL,
    amd numeric NOT NULL,
    byr numeric NOT NULL,
    bgn numeric NOT NULL,
    brl numeric NOT NULL,
    huf numeric NOT NULL,
    krw numeric NOT NULL,
    dkk numeric NOT NULL,
    usd numeric NOT NULL,
    eur numeric NOT NULL,
    inr numeric NOT NULL,
    kzt numeric NOT NULL,
    cad numeric NOT NULL,
    kgs numeric NOT NULL,
    cny numeric NOT NULL,
    lvl numeric NOT NULL,
    ltl numeric NOT NULL,
    mdl numeric NOT NULL,
    ron numeric NOT NULL,
    tmt numeric NOT NULL,
    nok numeric NOT NULL,
    pln numeric NOT NULL,
    xdr numeric NOT NULL,
    sgd numeric NOT NULL,
    tjs numeric NOT NULL,
    try numeric NOT NULL,
    uzs numeric NOT NULL,
    uah numeric NOT NULL,
    gbp numeric NOT NULL,
    czk numeric NOT NULL,
    sek numeric NOT NULL,
    chf numeric NOT NULL,
    zar numeric NOT NULL,
    jpy numeric NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: currency_courses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE currency_courses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: currency_courses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE currency_courses_id_seq OWNED BY currency_courses.id;


--
-- Name: product_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_categories (
    id integer NOT NULL,
    ext_id character varying NOT NULL,
    name character varying NOT NULL,
    "position" integer NOT NULL,
    active boolean NOT NULL,
    product_category_id integer,
    product_provider_id integer NOT NULL,
    translated_name character varying,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: product_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_categories_id_seq OWNED BY product_categories.id;


--
-- Name: product_categories_products; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_categories_products (
    id integer NOT NULL,
    product_id integer NOT NULL,
    product_category_id integer NOT NULL,
    product_provider_id integer NOT NULL
);


--
-- Name: product_categories_products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_categories_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_categories_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_categories_products_id_seq OWNED BY product_categories_products.id;


--
-- Name: product_images; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_images (
    id integer NOT NULL,
    ext_id character varying NOT NULL,
    active boolean NOT NULL,
    image_src character varying DEFAULT 'images/no_image.jpg'::character varying NOT NULL,
    product_id integer,
    product_provider_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: product_images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_images_id_seq OWNED BY product_images.id;


--
-- Name: product_providers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_providers (
    id integer NOT NULL,
    name character varying NOT NULL,
    currency currency NOT NULL,
    active boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: product_providers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_providers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_providers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_providers_id_seq OWNED BY product_providers.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE products (
    id integer NOT NULL,
    ext_id character varying NOT NULL,
    name character varying NOT NULL,
    price numeric NOT NULL,
    stock character varying NOT NULL,
    preorder boolean NOT NULL,
    limitation integer,
    rating real,
    description text NOT NULL,
    active boolean NOT NULL,
    product_id integer,
    product_provider_id integer NOT NULL,
    image_src character varying DEFAULT 'images/no_image.jpg'::character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: proxies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE proxies (
    id integer NOT NULL,
    ip character varying(15) NOT NULL,
    port character varying(6) NOT NULL,
    anonymity character(3) NOT NULL,
    protocol character varying(10) NOT NULL,
    country character(2) NOT NULL
);


--
-- Name: proxies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE proxies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: proxies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE proxies_id_seq OWNED BY proxies.id;


--
-- Name: related_products; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE related_products (
    id integer NOT NULL,
    related_id integer NOT NULL,
    product_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: related_products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE related_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: related_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE related_products_id_seq OWNED BY related_products.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: sessions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sessions (
    id integer NOT NULL,
    session_id character varying(255) NOT NULL,
    data text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sessions_id_seq OWNED BY sessions.id;


--
-- Name: shipping_infos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE shipping_infos (
    id integer NOT NULL,
    user_id integer,
    first_name character varying,
    last_name character varying,
    address character varying,
    city character varying,
    state character varying,
    country character varying,
    post_code character varying,
    phone character varying,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: shipping_infos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE shipping_infos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: shipping_infos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE shipping_infos_id_seq OWNED BY shipping_infos.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying NOT NULL,
    encrypted_password character varying NOT NULL,
    salt character varying NOT NULL,
    role user_role DEFAULT 'client'::user_role NOT NULL,
    balance money DEFAULT 0 NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY assemble_accounts ALTER COLUMN id SET DEFAULT nextval('assemble_accounts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cart_products ALTER COLUMN id SET DEFAULT nextval('cart_products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY carts ALTER COLUMN id SET DEFAULT nextval('carts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY currency_courses ALTER COLUMN id SET DEFAULT nextval('currency_courses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_categories ALTER COLUMN id SET DEFAULT nextval('product_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_categories_products ALTER COLUMN id SET DEFAULT nextval('product_categories_products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_images ALTER COLUMN id SET DEFAULT nextval('product_images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_providers ALTER COLUMN id SET DEFAULT nextval('product_providers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY proxies ALTER COLUMN id SET DEFAULT nextval('proxies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY related_products ALTER COLUMN id SET DEFAULT nextval('related_products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sessions ALTER COLUMN id SET DEFAULT nextval('sessions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY shipping_infos ALTER COLUMN id SET DEFAULT nextval('shipping_infos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: assemble_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY assemble_accounts
    ADD CONSTRAINT assemble_accounts_pkey PRIMARY KEY (id);


--
-- Name: cart_products_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY cart_products
    ADD CONSTRAINT cart_products_pkey PRIMARY KEY (id);


--
-- Name: carts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY carts
    ADD CONSTRAINT carts_pkey PRIMARY KEY (id);


--
-- Name: currency_courses_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY currency_courses
    ADD CONSTRAINT currency_courses_pkey PRIMARY KEY (id);


--
-- Name: product_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_categories
    ADD CONSTRAINT product_categories_pkey PRIMARY KEY (id);


--
-- Name: product_categories_products_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_categories_products
    ADD CONSTRAINT product_categories_products_pkey PRIMARY KEY (id);


--
-- Name: product_images_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_images
    ADD CONSTRAINT product_images_pkey PRIMARY KEY (id);


--
-- Name: product_providers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_providers
    ADD CONSTRAINT product_providers_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: proxies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY proxies
    ADD CONSTRAINT proxies_pkey PRIMARY KEY (id);


--
-- Name: related_products_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY related_products
    ADD CONSTRAINT related_products_pkey PRIMARY KEY (id);


--
-- Name: sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- Name: shipping_infos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY shipping_infos
    ADD CONSTRAINT shipping_infos_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_sessions_on_session_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_sessions_on_session_id ON sessions USING btree (session_id);


--
-- Name: index_sessions_on_updated_at; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_sessions_on_updated_at ON sessions USING btree (updated_at);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: assemble_accounts_product_provider_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY assemble_accounts
    ADD CONSTRAINT assemble_accounts_product_provider_id_fkey FOREIGN KEY (product_provider_id) REFERENCES product_providers(id);


--
-- Name: product_categories_product_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_categories
    ADD CONSTRAINT product_categories_product_category_id_fkey FOREIGN KEY (product_category_id) REFERENCES product_categories(id);


--
-- Name: product_categories_product_provider_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_categories
    ADD CONSTRAINT product_categories_product_provider_id_fkey FOREIGN KEY (product_provider_id) REFERENCES product_providers(id);


--
-- Name: product_categories_products_product_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_categories_products
    ADD CONSTRAINT product_categories_products_product_category_id_fkey FOREIGN KEY (product_category_id) REFERENCES product_categories(id);


--
-- Name: product_categories_products_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_categories_products
    ADD CONSTRAINT product_categories_products_product_id_fkey FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: product_categories_products_product_provider_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_categories_products
    ADD CONSTRAINT product_categories_products_product_provider_id_fkey FOREIGN KEY (product_provider_id) REFERENCES product_providers(id);


--
-- Name: product_images_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_images
    ADD CONSTRAINT product_images_product_id_fkey FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: product_images_product_provider_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_images
    ADD CONSTRAINT product_images_product_provider_id_fkey FOREIGN KEY (product_provider_id) REFERENCES product_providers(id);


--
-- Name: products_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_product_id_fkey FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: products_product_provider_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_product_provider_id_fkey FOREIGN KEY (product_provider_id) REFERENCES product_providers(id);


--
-- Name: related_products_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY related_products
    ADD CONSTRAINT related_products_product_id_fkey FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: related_products_related_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY related_products
    ADD CONSTRAINT related_products_related_id_fkey FOREIGN KEY (related_id) REFERENCES products(id);


--
-- PostgreSQL database dump complete
--

INSERT INTO schema_migrations (version) VALUES ('20130224093135');

INSERT INTO schema_migrations (version) VALUES ('20130224093136');

INSERT INTO schema_migrations (version) VALUES ('20130224093137');

INSERT INTO schema_migrations (version) VALUES ('20130224093138');

INSERT INTO schema_migrations (version) VALUES ('20130224093139');

INSERT INTO schema_migrations (version) VALUES ('20130224093140');

INSERT INTO schema_migrations (version) VALUES ('20130224093141');

INSERT INTO schema_migrations (version) VALUES ('20130224093142');

INSERT INTO schema_migrations (version) VALUES ('20130224093143');

INSERT INTO schema_migrations (version) VALUES ('20130309161706');

INSERT INTO schema_migrations (version) VALUES ('20130311201205');

INSERT INTO schema_migrations (version) VALUES ('20130315155503');

INSERT INTO schema_migrations (version) VALUES ('20130317101643');

INSERT INTO schema_migrations (version) VALUES ('20130317101644');

INSERT INTO schema_migrations (version) VALUES ('20130317101645');

INSERT INTO schema_migrations (version) VALUES ('20130317101646');