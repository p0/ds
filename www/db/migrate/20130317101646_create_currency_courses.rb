class CreateCurrencyCourses < ActiveRecord::Migration

    def up
        execute <<-SQL
            CREATE TABLE currency_courses (
                id                      serial                      PRIMARY KEY,
                aud                     numeric         NOT NULL,
                azn                     numeric         NOT NULL,
                amd                     numeric         NOT NULL,
                byr                     numeric         NOT NULL,
                bgn                     numeric         NOT NULL,
                brl                     numeric         NOT NULL,
                huf                     numeric         NOT NULL,
                krw                     numeric         NOT NULL,
                dkk                     numeric         NOT NULL,
                usd                     numeric         NOT NULL,
                eur                     numeric         NOT NULL,
                inr                     numeric         NOT NULL,
                kzt                     numeric         NOT NULL,
                cad                     numeric         NOT NULL,
                kgs                     numeric         NOT NULL,
                cny                     numeric         NOT NULL,
                lvl                     numeric         NOT NULL,
                ltl                     numeric         NOT NULL,
                mdl                     numeric         NOT NULL,
                ron                     numeric         NOT NULL,
                tmt                     numeric         NOT NULL,
                nok                     numeric         NOT NULL,
                pln                     numeric         NOT NULL,
                xdr                     numeric         NOT NULL,
                sgd                     numeric         NOT NULL,
                tjs                     numeric         NOT NULL,
                try                     numeric         NOT NULL,
                uzs                     numeric         NOT NULL,
                uah                     numeric         NOT NULL,
                gbp                     numeric         NOT NULL,
                czk                     numeric         NOT NULL,
                sek                     numeric         NOT NULL,
                chf                     numeric         NOT NULL,
                zar                     numeric         NOT NULL,
                jpy                     numeric         NOT NULL,
                created_at              timestamp       NOT NULL    DEFAULT now(),
                updated_at              timestamp       NOT NULL    DEFAULT now()
            );
        SQL
    end
    
    def down
        execute "DROP TABLE currency_courses"
    end
end
