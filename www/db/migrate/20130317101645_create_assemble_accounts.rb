class CreateAssembleAccounts < ActiveRecord::Migration

    def up
        execute <<-SQL
            CREATE TABLE assemble_accounts (
                id                      serial                      PRIMARY KEY,
                email                   varchar         NOT NULL,
                password                varchar         NOT NULL,
                name                    varchar,
                surname                 varchar,
                dob                     date,
                phone                   varchar,
                gender                  boolean,
                address                 varchar,
                city                    varchar,
                state                   varchar,
                country                 country,
                postcode                varchar,
                product_provider_id     integer         NOT NULL    REFERENCES product_providers(id),
                created_at              timestamp       NOT NULL    DEFAULT now(),
                updated_at              timestamp       NOT NULL    DEFAULT now()
            );

            INSERT INTO assemble_accounts (
                email, password, name, surname, 
                dob, phone, gender, 
                address, city, state, country, postcode, product_provider_id
            ) VALUES (
                'maricher1989@gmail.com', 'sNCQtGIX7s', 'Marina', 'Chernyh', 
                '1989-03-09', '+78605547586', false, 
                'Yralskaya, 5 - 91', 'Novgorod', 'Novgorodskaya', 'RU', '619282', 1
            ), (
                'denibegu1984@gmail.com', 'cESFR6kFw9', 'Denis', 'Begun', 
                '1984-02-19', '+74827425248', false, 
                'Lenina, 3 - 29', 'Astrahan', 'Astrahanskaya', 'RU', '754564', 1
            ), (
                'irinsolo1993@gmail.com', 'enhIoGO81W', 'Irina', 'Solovey', 
                '1985-01-18', '+77138187602', false, 
                'Naberejnaya, 18 - 26', 'Belgorod', 'Belgorodskaya', 'RU', '542247', 1
            );
        SQL
    end
    
    def down
        execute "DROP TABLE assemble_accounts"
    end
end
