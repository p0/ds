class CreateUsers < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE TABLE users (
        id                  serial                      PRIMARY KEY,
        email               varchar         NOT NULL,
        encrypted_password  varchar         NOT NULL,
        salt                varchar         NOT NULL,
        role                user_role       NOT NULL    DEFAULT 'client',
        balance             money           NOT NULL    DEFAULT 0,
        created_at          timestamp       NOT NULL    DEFAULT now(),
        updated_at          timestamp       NOT NULL    DEFAULT now()
      )
    SQL
  end

  def down
    execute "DROP TABLE users"
  end
end 