class CreateCurrency < ActiveRecord::Migration
  def up
    execute "CREATE TYPE currency AS ENUM (
        'mdl', 'kgs', 'inr', 
        'jpy', 'uah', 'brl', 
        'sek', 'sgd', 'uzs', 
        'usd', 'tmt', 'aud', 
        'chf', 'zar', 'byr', 
        'cny', 'eur', 'huf', 
        'kzt', 'nok', 'amd', 
        'tjs', 'xdr', 'pln', 
        'azn', 'czk', 'krw', 
        'bgn', 'try', 'gbp', 
        'lvl', 'ron', 'cad', 
        'dkk', 'ltl'
    )"
  end

  def down
    execute "DROP TYPE currency"
  end
end 