class CreateShippingInfos < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE TABLE shipping_infos (
        id                  serial                      PRIMARY KEY,
        user_id				      integer,
        first_name          varchar,
        last_name           varchar,
        address             varchar,
        city                varchar,
        state               varchar,
        country             varchar,
        post_code           varchar,
        phone               varchar,
        created_at          timestamp       NOT NULL    DEFAULT now(),
        updated_at          timestamp       NOT NULL    DEFAULT now()
      )
    SQL
  end

  def down
    execute "DROP TABLE shipping_infos"
  end
end 