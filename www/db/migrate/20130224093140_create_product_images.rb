class CreateProductImages < ActiveRecord::Migration
    def up
        execute <<-SQL
            CREATE TABLE product_images (
                id                      serial                      PRIMARY KEY,
                ext_id                  varchar         NOT NULL,
                active                  boolean         NOT NULL,
                image_src               varchar         NOT NULL    DEFAULT 'images/no_image.jpg',
                product_id              integer                     REFERENCES products(id),
                product_provider_id     integer         NOT NULL    REFERENCES product_providers(id),
                created_at              timestamp       NOT NULL    DEFAULT now(),
                updated_at              timestamp       NOT NULL    DEFAULT now()
            )
        SQL
    end

    def down
        execute "DROP TABLE product_images"
    end
end 