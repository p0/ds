class CreateProducts < ActiveRecord::Migration
    def up
        execute <<-SQL
            CREATE TABLE products (
                id                      serial                      PRIMARY KEY,
                ext_id                  varchar         NOT NULL,
                name                    varchar         NOT NULL,
                price                   numeric         NOT NULL,
                stock                   varchar         NOT NULL,
                preorder                boolean         NOT NULL,
                limitation              integer,
                rating                  real,
                description             text            NOT NULL,
                active                  boolean         NOT NULL,
                product_id              integer                     REFERENCES products(id),
                product_provider_id     integer         NOT NULL    REFERENCES product_providers(id),
                image_src               varchar         NOT NULL    DEFAULT 'images/no_image.jpg',
                created_at              timestamp       NOT NULL    DEFAULT now(),
                updated_at              timestamp       NOT NULL    DEFAULT now()
            )
        SQL
    end

    def down
        execute "DROP TABLE products"
    end
end 