class CreateUserRole < ActiveRecord::Migration
  def up
    execute "CREATE TYPE user_role AS ENUM ('admin', 'client')"
  end

  def down
    execute "DROP TYPE user_role"
  end
end 