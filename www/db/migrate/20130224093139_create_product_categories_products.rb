class CreateProductCategoriesProducts < ActiveRecord::Migration
    def up
        execute <<-SQL
            CREATE TABLE product_categories_products (
                id                      serial                  PRIMARY KEY,
                product_id              integer     NOT NULL    REFERENCES products(id),
                product_category_id     integer     NOT NULL    REFERENCES product_categories(id),
                product_provider_id     integer     NOT NULL    REFERENCES product_providers(id)
            )
        SQL
    end

    def down
        execute "DROP TABLE product_categories_products"
    end
end
