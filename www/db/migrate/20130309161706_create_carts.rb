class CreateCarts < ActiveRecord::Migration
  
  def up
    execute <<-SQL
      CREATE TABLE carts (
        id                  serial                      PRIMARY KEY,
        user_id				      integer,
        created_at          timestamp       NOT NULL    DEFAULT now(),
        updated_at          timestamp       NOT NULL    DEFAULT now()
      )
    SQL
  end

  def down
    execute "DROP TABLE carts"
  end

end

