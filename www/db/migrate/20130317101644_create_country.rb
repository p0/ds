class CreateCountry < ActiveRecord::Migration
  def up
    execute "CREATE TYPE country AS ENUM ('RU', 'US', 'YA', 'DE')"
  end

  def down
    execute "DROP TYPE country"
  end
end 