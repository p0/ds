class CreateRelatedProducts < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE TABLE related_products (
        id              serial                      PRIMARY KEY,
        related_id      integer         NOT NULL    REFERENCES products(id),
        product_id      integer         NOT NULL    REFERENCES products(id),
        created_at      timestamp       NOT NULL    DEFAULT now(),
        updated_at      timestamp       NOT NULL    DEFAULT now()
      )
    SQL
  end

  def down
    execute "DROP TABLE related_products"
  end
end 