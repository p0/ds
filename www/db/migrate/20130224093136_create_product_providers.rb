class CreateProductProviders < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE TABLE product_providers (
        id              serial                      PRIMARY KEY,
        name            varchar         NOT NULL,
        currency        currency        NOT NULL,
        active          boolean         NOT NULL    DEFAULT FALSE,
        created_at      timestamp       NOT NULL    DEFAULT now(),
        updated_at      timestamp       NOT NULL    DEFAULT now()
      );

      INSERT INTO product_providers (name, currency, active)
      VALUES 
        ('hs', 'jpy', true),
        ('ami', 'usd', true);
    SQL
  end

  def down
    execute "DROP TABLE product_providers"
  end
end 