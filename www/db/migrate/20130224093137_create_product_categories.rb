class CreateProductCategories < ActiveRecord::Migration
    def up
        execute <<-SQL
            CREATE TABLE product_categories (
                id                      serial                      PRIMARY KEY,
                ext_id                  varchar         NOT NULL,
                name                    varchar         NOT NULL,
                position                integer         NOT NULL,
                active                  boolean         NOT NULL,
                product_category_id     integer                     REFERENCES product_categories(id),
                product_provider_id     integer         NOT NULL    REFERENCES product_providers(id),
                translated_name         varchar,
                created_at              timestamp       NOT NULL    DEFAULT now(),
                updated_at              timestamp       NOT NULL    DEFAULT now()
            )
        SQL
    end

    def down
        execute "DROP TABLE product_categories"
    end
end 