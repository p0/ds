class CreateCartProducts < ActiveRecord::Migration

     def up
        execute <<-SQL
            CREATE TABLE cart_products (
                id                      serial                      PRIMARY KEY,
                cart_id                 integer         NOT NULL,
                product_id              integer         NOT NULL,
                quantity                integer         NOT NULL,
                created_at              timestamp       NOT NULL    DEFAULT now(),
                updated_at              timestamp       NOT NULL    DEFAULT now()
            )
        SQL
    end
    
    def down
        execute "DROP TABLE cart_products"
    end
end
