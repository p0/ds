# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130225103101) do

  create_table "product_categories", :force => true do |t|
    t.string   "ext_id",          :limit => nil, :null => false
    t.string   "name",            :limit => nil, :null => false
    t.integer  "position",                       :null => false
    t.boolean  "active",                         :null => false
    t.integer  "category_id"
    t.integer  "provider_id",                    :null => false
    t.string   "translated_name", :limit => nil
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "product_images", :force => true do |t|
    t.string   "ext_id",      :limit => nil, :null => false
    t.string   "attr",        :limit => 10
    t.boolean  "active",                     :null => false
    t.string   "src",         :limit => nil, :null => false
    t.integer  "product_id"
    t.integer  "provider_id",                :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "product_parents", :force => true do |t|
    t.integer "product_id",  :null => false
    t.integer "category_id", :null => false
    t.integer "provider_id", :null => false
  end

# Could not dump table "product_providers" because of following StandardError
#   Unknown type 'currency' for column 'currency'

  create_table "products", :force => true do |t|
    t.string   "ext_id",      :limit => nil,                                :null => false
    t.string   "name",        :limit => nil,                                :null => false
    t.decimal  "price",                      :precision => 19, :scale => 2, :null => false
    t.string   "stock",       :limit => 10,                                 :null => false
    t.boolean  "backorder",                                                 :null => false
    t.float    "rating",                                                    :null => false
    t.text     "description"
    t.boolean  "active",                                                    :null => false
    t.integer  "product_id"
    t.integer  "provider_id",                                               :null => false
    t.datetime "created_at",                                                :null => false
    t.datetime "updated_at",                                                :null => false
  end

  create_table "proxies", :force => true do |t|
    t.string "ip",        :limit => 15, :null => false
    t.string "port",      :limit => 6,  :null => false
    t.string "anonymity", :limit => 3,  :null => false
    t.string "protocol",  :limit => 10, :null => false
    t.string "country",   :limit => 2,  :null => false
  end

  create_table "related_products", :force => true do |t|
    t.integer  "related_id", :null => false
    t.integer  "product_id", :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

# Could not dump table "users" because of following StandardError
#   Unknown type 'user_role' for column 'role'

end
