require 'test_helper'

class ProductProvidersControllerTest < ActionController::TestCase
  setup do
    @product_provider = product_providers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_providers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_provider" do
    assert_difference('ProductProvider.count') do
      post :create, product_provider: {  }
    end

    assert_redirected_to product_provider_path(assigns(:product_provider))
  end

  test "should show product_provider" do
    get :show, id: @product_provider
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_provider
    assert_response :success
  end

  test "should update product_provider" do
    put :update, id: @product_provider, product_provider: {  }
    assert_redirected_to product_provider_path(assigns(:product_provider))
  end

  test "should destroy product_provider" do
    assert_difference('ProductProvider.count', -1) do
      delete :destroy, id: @product_provider
    end

    assert_redirected_to product_providers_path
  end
end
