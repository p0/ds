module SessionsHelper
    
    def sign_in(user)
        cookies.permanent.signed[:remember_token] = [user.id, user.salt]
        current_user = user
	    
        # ситуация, когда зареганный юзер с пустой корзиной собирает корзину гостем и логинется
	    if !cookies[:cart_id].nil?                                         # Если да, то есть ли куки от гостя?
	        @cart = Cart.find_by_id(cookies.signed[:cart_id])
	        if @cart.products.count == 0
	          cookies.delete(:cart_id)
	        end
	        @cart = current_user.cart                                      # Если да, то может быть
	        if @cart.products.count == 0                                   # уже были товары в корзине?
		        @cart = Cart.find_by_id(cookies.signed[:cart_id])          # если там пусто, то берем корзину госься
		        current_user.cart = @cart                                  # и отдаем карренту
		        cookies.delete(:cart_id)								   # и стираем куки гостя
		    end
		end
	    
    end

    def sign_out
        cookies.delete(:remember_token)
        cookies.delete(:cart_id)
        current_user = nil
    end

    def current_user=(user)
        @current_user = user
    end

    def signed_in?
        !current_user.nil?
    end

    def current_user
        @current_user ||= user_from_remember_token
    end

    private

        def user_from_remember_token
            User.authenticate_with_salt(*remember_token)
        end

        def remember_token
            cookies.signed[:remember_token] || [nil, nil]
        end
end
