module ProductCategoriesHelper
    def categories_for
        categories = ProductCategory.where active: true
        categories_list = []

        categories.each do |category|
            category_link = link_to category.name, product_category_products_path(category.id)
            categories_list << content_tag(:li, category_link)
        end

        content_tag :ul, categories_list.join("").html_safe, class: "nav nav-tabs nav-stacked"
    end
end
