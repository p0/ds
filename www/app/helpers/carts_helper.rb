module CartsHelper

	def find_cart_product(cart)
	    @cart = cart
	    @cart_products = @cart.cart_products.order('created_at').includes :product
	end

	def cart_from_cookies
	    Cart.find cookies.signed[:cart_id]
	end

end
