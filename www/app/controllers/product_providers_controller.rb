class ProductProvidersController < ApplicationController
  # GET /product_providers
  # GET /product_providers.json
  def index
    @product_providers = ProductProvider.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @product_providers }
    end
  end

  # GET /product_providers/1
  # GET /product_providers/1.json
  def show
    @product_provider = ProductProvider.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product_provider }
    end
  end

  # GET /product_providers/new
  # GET /product_providers/new.json
  def new
    @product_provider = ProductProvider.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product_provider }
    end
  end

  # GET /product_providers/1/edit
  def edit
    @product_provider = ProductProvider.find(params[:id])
  end

  # POST /product_providers
  # POST /product_providers.json
  def create
    @product_provider = ProductProvider.new(product_provider_params)

    respond_to do |format|
      if @product_provider.save
        format.html { redirect_to @product_provider, notice: 'Product provider was successfully created.' }
        format.json { render json: @product_provider, status: :created, location: @product_provider }
      else
        format.html { render action: "new" }
        format.json { render json: @product_provider.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /product_providers/1
  # PUT /product_providers/1.json
  def update
    @product_provider = ProductProvider.find(params[:id])

    respond_to do |format|
      if @product_provider.update_attributes(product_provider_params)
        format.html { redirect_to @product_provider, notice: 'Product provider was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @product_provider.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_providers/1
  # DELETE /product_providers/1.json
  def destroy
    @product_provider = ProductProvider.find(params[:id])
    @product_provider.destroy

    respond_to do |format|
      format.html { redirect_to product_providers_url }
      format.json { head :no_content }
    end
  end
  def product_provider_params
    params.require(:product_provider).permit(:name, :currency, :active)
  end
end
