class MakeAChoicesController < ApplicationController
  
  #GET /cart
  def index                                                                  
      @user = current_user                                            # |
      @cart = current_user.cart                                       # |=> корзина с базы                         
      @products = @cart.products                                      # |                               
      @cart_from_cookies = Cart.find cookies.signed[:cart_id]         #   | => корзина с куки
      @products_from_cookies = @cart_from_cookies.products            #   |
  end
end
 