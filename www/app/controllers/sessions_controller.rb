class SessionsController < ApplicationController
  include SessionsHelper

  #GET /signin
  def new
    @title = "Sign in"
  end

  #POST /sessions
  def create
    user = User.authenticate(params[:session][:email], params[:session][:password])
    if user.nil?
      respond_to do |format|
        format.html { redirect_to "/signin", notice: 'Invalid login/password combination.'}
      end
    else
      sign_in user
      redirect_to "/cart"
    end
  end
  
  #DELETE /signout
  def destroy
    sign_out
    redirect_to "/users"
  end
end