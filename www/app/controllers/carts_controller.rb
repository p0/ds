class CartsController < ApplicationController
  include CartsHelper
=begin
  В начале проверяется залогинен пользователь или нет. Если не залогинен, то выводится корзина гостя,
  которая хранится в куки. Если мы залогинены, то проверяем есть ли у нас корзина, соранная гостем.
  Естестественно если нет, то мы выводим с базы. Если все же у нас 2 корзины(в базе и в куки), то тут 
  есть 2 варианта: 
    1) товары в них совпадают. В этом случае сохраняем корзину с базы и выводим ее.
    2) товары в них разные. Тут мы отправляем юзера на страницу выбора между двумя корзинами.
       И в зависимости от выбора сохраняем ту или иную корзину.
=end

  # GET /cart
  def show
    # залогинен?
    if signed_in?                                       
      @user = current_user
      # есть ли корзина, собранная гостем?
      if !cookies[:cart_id].nil?                        
        @cart = current_user.cart                       
        @cart_cookies = cart_from_cookies
        # если товары в корзинах (с базы и с куки) совпадают                                      
        if (@cart.products.count != 0) && (@cart.products == @cart_cookies.products)               
          # то выводим только с базы
          current_user.cart = @cart                   
          find_cart_product(@cart)                    
          cookies.delete(:cart_id)                    
        else
          # иначе предлагаем сделать выбор
          case (params[:options])                     
          when "1"
            @cart = current_user.cart                 
            current_user.cart = @cart                                  
            find_cart_product(@cart)
            cookies.delete(:cart_id)                  
          when "2"
            @cart = cart_from_cookies                  
            current_user.cart = @cart            
            find_cart_product(@cart)
            cookies.delete(:cart_id)
          when nil                                    
            redirect_to "/make_a_choice"              
          end
        end
      else
        # выводим с базы
        @cart = current_user.cart                       
        find_cart_product(@cart)                        
      end
    else
      # выводим с куки
      @cart = cart_from_cookies                         
      find_cart_product(@cart)                          
    end

    
  end

  # POST /add_product
  def new
    # если залогинены, берем корзину из базы
    if signed_in?
      cart = current_user.cart
    else
      # если нет кук - создаем новую корзину
      if cookies[:cart_id].nil?
        cart = Cart.create
        cookies.signed[:cart_id] = cart.id

      # есть куки - берем корзину из кук
      else
        cart = cart_from_cookies
      end
    end   

    # добавляем товар в корзину 
    # или увеличиваем количество на 1, если он уже там
    product = cart.cart_products.find_by_product_id params[:id]
    if product
      product.update_attribute :quantity, product.quantity + 1
    else
      CartProduct.create product_id: params[:id], cart_id: cart.id, quantity: 1
    end      
    redirect_to "/cart" 
  end

  def update
    # достаем корзину
    cart = signed_in? ? current_user.cart : cart_from_cookies
    # просматриваем товары
    params[:product].each do |id, state|
      # удаляем отмеченные
      if !state[:deleted].nil?
        cart.cart_products.find_by_product_id(id).destroy
        next
      end

      # обновляем количество, если изменено
      if state[:updated_qty] != state[:initial_qty]
        cart.cart_products.find_by_product_id(id).update_attribute :quantity, state[:updated_qty]
      end
    end

    redirect_to "/cart"
  end
end
 