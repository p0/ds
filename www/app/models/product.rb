class Product < ActiveRecord::Base
    has_many                :cart_products
    has_many                :carts, through: :cart_products
    has_many                :product_images, inverse_of: :product
    has_and_belongs_to_many :product_categories
=begin   
    attr_accessible         :active
    attr_accessible         :backorder
    attr_accessible         :description
    attr_accessible         :ext_id
    attr_accessible         :name
    attr_accessible         :price
    attr_accessible         :provider_id
    attr_accessible         :rating
    attr_accessible         :stock
    attr_accessible         :variation
=end
    def self.search(search, page)
        paginate per_page: 16, page: page, order: :id
    end
end
