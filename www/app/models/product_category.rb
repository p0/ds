class ProductCategory < ActiveRecord::Base
    has_and_belongs_to_many :products
=begin    
    attr_accessible :category_id
    attr_accessible :ext_id
    attr_accessible :name
    attr_accessible :translated_name
    attr_accessible :cart_products
=end
    def self.active_products
        products.where active: true
    end
end
