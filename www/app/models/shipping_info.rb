class ShippingInfo < ActiveRecord::Base
  
  belongs_to      :user
=begin
  attr_accessible :first_name
  attr_accessible :user_id
  attr_accessible :last_name
  attr_accessible :address
  attr_accessible :city
  attr_accessible :state
  attr_accessible :country
  attr_accessible :post_code
  attr_accessible :phone
=end

  validates :first_name,  :presence => true, 
                          :length => { :within => 2..40 }
                   
  validates :last_name,   :presence => true, 
                          :length => { :within => 2..40 }

  validates :address,     :presence => true, 
                          :length => { :within => 2..40 }

  validates :city,        :presence => true, 
                          :length => { :within => 2..40 }

  validates :state,       :presence => true, 
                          :length => { :within => 2..40 }

  validates :country,     :presence => true, 
                          :length => { :within => 2..40 }

  validates :post_code,   :presence => true, 
                          :length => { :within => 5..6 },
                          :numericality => { :only_integer => true }

  validates :phone,       :presence => true, 
                          :length => { :within => 6..40 },
                          :numericality => true       

end
