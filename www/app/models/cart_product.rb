class CartProduct < ActiveRecord::Base
  
  	#attr_accessible :cart_id, :product_id, :quantity
  	belongs_to 		:cart
 	belongs_to 		:product 


 	validates :quantity, { numericality: { greater_than: 0 } }
end
