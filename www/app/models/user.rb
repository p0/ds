class User < ActiveRecord::Base

  has_one         :cart
  has_one         :shipping_info
  before_save     :encrypt_password
  
  attr_accessor   :password
=begin
  attr_accessible :password, :password_confirmation

  attr_accessible :user, :user_id
  attr_accessible :email
  attr_accessible :role
  attr_accessible :balance
=end


  validates :password,    :presence     => true,
                          :confirmation => true,
                          :length       => { :within => 6..40 }

  validates :email,       :presence => true, 
                          :length => { :within => 5..40 },
                          :uniqueness => true
                          # жалуется на мультиплай, поправить!
                          #:format => {:with => /\a([\a@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}

  
  validates :role,        :presence => true, 
                          :length => { :within => 2..40 }

  validates :balance,     :presence => true


                          
  def self.search_user(search)
      if search
        find(:all, :conditions => ['email LIKE ?', "%#{search}%"])
      else
        find(:all)
      end
  end 

  def self.authenticate(email, submitted_password)
      user = find_by_email(email)
      return nil  if user.nil?
      return user if user.has_password?(submitted_password)
  end

  def self.authenticate_with_salt(id, cookie_salt)
      user = find_by_id(id)
      (user && user.salt == cookie_salt) ? user : nil
  end

  def has_password?(submitted_password)
      encrypted_password == encrypt(submitted_password)
  end

  private

    def encrypt_password
        self.salt = make_salt if new_record?
        self.encrypted_password = encrypt(password)
    end

    def encrypt(string)
        secure_hash("#{salt}--#{string}")
    end

    def make_salt
        secure_hash("#{Time.now.utc}--#{password}")
    end

    def secure_hash(string)
        Digest::SHA2.hexdigest(string)
    end 


end


