# -*- coding: UTF-8 -*-

import logging
from decimal import Decimal
from lxml.html import clean
from updating.base import Elements
from common.base import blue, green, yell, red, mag, Element


log = logging.getLogger('product')


# обработка описания
def procDescription(description):
    # на всякий случай заворачиваем описание в дополнительный div
    description = '<div>%s</div>' % description

    # убираем комментарии и все аттрибуты кроме 'src', 'href'
    safe_attrs = clean.defs.safe_attrs
    clean.defs.safe_attrs = frozenset(('src', 'href'))
    cleaner = clean.Cleaner(safe_attrs_only=True)
    return cleaner.clean_html(description)


class Products(Elements):
    def __init__(self, context, connection, categories):
        super().__init__(context, connection)
        self.categories = categories
        # родительские категории товаров {product_id: {product_category_id: id}}
        self.parents = dict()

        self.cursor.execute('SELECT * FROM product_categories_products WHERE product_provider_id = %s', [self.context.providerInfo.id])
        for data in self.cursor.fetchall():
            el = Element(data)
            if el.product_id not in self.parents:
                self.parents[el.product_id] = dict()
            self.parents[el.product_id][el.product_category_id] = el.id


    # загрузка товаров из базы
    def loadExisting(self):
        getProducts = '''
            SELECT 
                id, ext_id, name, price, stock, 
                preorder, limitation, rating, description, 
                active, product_provider_id, image_src
            FROM products
            WHERE product_provider_id = %s AND product_id IS NULL
        '''

        log.info('loadExisting(): started')
        self.cursor.execute(getProducts, [self.context.providerInfo.id])
        for data in self.cursor.fetchall():
            el = Element(data)
            self.existing[el.ext_id] = el
        log.info('loadExisting(): finished')


    # обработка товаров из обновления
    def procUpdated(self):
        log.info('procUpdated(): started')
        for product in self.updated.values():
            # log.debug("procUpdated(): ext_id: '%s'" % yell(product.ext_id))
            # проверяем количество на складе, т.к. не сможем проверить на уровне базы
            if product.stock not in ('+', '-'):
                try:
                    int(product.stock)
                except ValueError:
                    raise UpdateError("Incorrect stock '%s'!" % product.stock)

            # цена
            product.price = Decimal(product.price)
            # предзаказ
            product.preorder = bool(product.preorder)
            # ограничение заказа (не обязательное поле)
            product.limitation = float(product.limitation) if product.get('limitation') else None
            # рейтинг (не обязательное поле)
            product.rating = float(product.rating) if product.get('rating') else None
            # описание
            product.description = procDescription(product.description)
            # изображение
            product.image_src = self.procImage(product.image_ext_id, adapt=True)

        log.info('procUpdated(): finished')


    # поиск измененных товаров 
    def searchChanged(self):
        attribs = (
            'name', 'price', 'rating', 'stock', 'preorder', 
            'limitation', 'description', 'active', 'image_src'
        )
        super().searchChanged(attribs)


    # обновление измененных товаров
    def updateChanged(self):
        updateProduct = '''
            UPDATE products 
            SET 
                name = %(name)s, 
                price = %(price)s, 
                stock = %(stock)s, 
                preorder = %(preorder)s,
                limitation = %(limitation)s,
                rating = %(rating)s, 
                description = %(description)s, 
                active = %(active)s,
                image_src = %(image_src)s,
                updated_at = now()

            WHERE id = %(id)s
        '''

        log.info('updateChanged(): started')
        for ext_id in self.changed:
            self.cursor.execute(updateProduct, self.updated[ext_id])
        log.info('updateChanged(): finished')


    # исключение товаров
    def excludeDeleted(self):
        excludeProduct = '''
            UPDATE products
            SET 
                active = FALSE,
                updated_at = now()
            WHERE id = %(id)s
        '''

        log.info('excludeDeleted(): started')
        for ext_id in self.deleted:
            self.cursor.execute(excludeProduct, self.existing[ext_id])
            log.debug('exclude product id: %s, ext_id: %s' % yell(self.existing[ext_id], ext_id))
        log.info('excludeDeleted(): finished')


    # добавление новых товаров
    def addNew(self):
        addProduct = '''
            INSERT INTO products (
                name, ext_id, price, preorder, 
                limitation, stock, rating, description, 
                active, product_provider_id, image_src
            ) VALUES (
                %(name)s, %(ext_id)s, %(price)s, %(preorder)s, 
                %(limitation)s, %(stock)s, %(rating)s, %(description)s, 
                %(active)s, %(product_provider_id)s, %(image_src)s
            )
            RETURNING id
        '''

        log.info('addNew(): started')
        for ext_id in self.added:
            self.cursor.execute(addProduct, self.updated[ext_id])
            id = self.cursor.fetchone()[0]
            self.updated[ext_id].id = id
            log.debug('add new product id: %s, ext_id: %s' % yell(id, ext_id))
        log.info('addNew(): finished')


    # поиск перемещенных товаров 
    def searchMoved(self):
        log.info('searchMoved(): started')

        # идем по товарам из обновления
        for ext_id in self.updated:
            up = self.updated[ext_id]

            # товар может быть в нескольких категориях,
            # поэтому parent_id в общем случае - список ext_id родительских категорий,
            # но если товар только в одной категории, допускается писать просто ext_id родительской категории
            # в этом случае заворачиваем этот ext_id в список
            if not isinstance(up.parent_id, (list, tuple)):
                up.parent_id = [up.parent_id]

            # заносим id (НЕ ext_id!) родительских категорий товара в множество parents товара
            # не путать up.parents и self.parents!
            up.parents = set()
            for parent in up.parent_id:
                up.parents.add(self.categories.updated[parent].id)

        # идем по оставшимся товарам (присутсвуют как в базе, так и в обновлении)
        for ext_id in self.undeleted:
            up = self.updated[ext_id]
            log.debug("searchMoved(): id: %s, ext_id: '%s'" % yell(up.id, up.ext_id))

            # если изменен список родительских категорий
            if up.parents != self.parents[up.id].keys():
                log.debug("searchMoved(): %s -> %s" % yell(up.parents, self.parents[up.id].keys()))
                self.moved.add(ext_id)
        log.info('searchMoved(): finished')


    def setParents(self):
        addParent = '''
            INSERT INTO product_categories_products (product_id, product_category_id, product_provider_id)
            VALUES (%s, %s, %s)
        '''

        log.info('setParents(): started')
        for ext_id in self.moved:
            up = self.updated[ext_id]
            addedCategories = up.parents - self.parents[up.id].keys()
            deletedCategories = self.parents[up.id].keys() - up.parents

            for parent in addedCategories:
                self.cursor.execute(addParent, [up.id, self.categories.identified[parent].id, self.context.providerInfo.id])

        # добавляем родителей
        for ext_id in self.added:
            # print ('ext_id: %s' % ext_id)
            up = self.updated[ext_id]
            # print('parent_id %s' % str(up.parent_id))
            for parent in up.parents:
                log.debug('set parent id: %s, ext_id: %s for id: %s, ext_id: %s' % 
                    yell(parent, self.categories.identified[parent].ext_id, up.id, ext_id))
                self.cursor.execute(addParent, [up.id, parent, self.context.providerInfo.id])

        log.info('setParents(): finished')



