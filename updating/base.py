import os
import logging
import psycopg2
import psycopg2.extras
from decimal import Decimal, getcontext
from urllib.parse import urlparse
from PIL import Image
from common.base import blue, green, yell, red, mag

log = logging.getLogger('base')
getcontext().prec = 2

noImagePath = 'images/no_image.jpg'


# ошибки при обновлении
class UpdateError(Exception):
    def __init__(self, message, element):
        self.message = message
        self.element = element

    # def __str__(self):
    #     return self.element
    #     return '%s %s' % (self.message, 'ololo')


    # def __repr__(self):
    #     return "ololo"


class Elements:
    def __init__(self, context, connection):
        self.context = context
        self.connection = connection
        self.cursor = self.connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # словари {ext_id: Element, ...} 
        self.existing = dict()          # данные из базы
        self.updated = dict()           # данные из обновления

        # словарь {id: Element, ...}
        self.identified = dict()

        # множества ext_id'шников
        self.added = set()              # добавленные данные (есть в обновлении, но нет в базе)
        self.deleted = set()            # удаленные данные (есть в базе, но нет в обновлении)
        self.undeleted = set()          # оставшиеся данные (есть в базе, остались и в обновлении)
        self.changed = set()            # измененные данные (среди оставшихся - в обновлении изменено значение хотя бы одного поля)
        self.moved = set()              # перемещенные данные (среди оставшихся - в обновлении изменен родитель)


    # определение, какие данные были добавлены, удалены и оставлены 
    def fillSets(self):
        self.added = set(self.updated) - set(self.existing)
        self.deleted = set(self.existing) - set(self.updated)
        self.undeleted = set(self.updated) - set(self.added)


    # загрузка данных из базы
    # заполненяется self.existing
    def loadExisting(self):
        raise Exception('Implement requared!')


    # обработка данных из обновления
    # заполняется self.updated
    def procUpdated(self):
        raise Exception('Implement requared!')


    # поиск измененных данных 
    # заполняется self.changed
    def searchChanged(self, attribs):
        log.info('searchChanged(): started')
        for ext_id in self.undeleted:
            up = self.updated[ext_id]
            ex = self.existing[ext_id]
            # log.debug("searchChanged(): id: %s, ext_id: '%s'" % yell(ex.id, ex.ext_id))

            # сохраняем идентификатор в обновлении
            up.id = ex.id

            # ищем изменения среди указанных полей
            for attr in attribs:
                if up[attr] != ex[attr]:
                    log.debug("searchChanged(): %s '%s' -> '%s'" % yell(attr, ex[attr], up[attr]))
                    self.changed.add(ext_id)
        log.info('searchChanged(): finished')


    # обновление измененных данных
    # заталкиваем self.changed в базу
    def updateChanged(self):
        raise Exception('Implement requared!')


    # исключение данных
    # для товаров из self.deleted проставляем в базе поле active = FALSE
    # товары из базы не удаляются!
    def excludeDeleted(self):
        raise Exception('Implement requared!')


    # добавление новых данных
    # добавляем товары из self.added в базу
    def addNew(self):
        raise Exception('Implement requared!')


    # получение локальных id'шников для товаров из обновления
    # заполняется self.identified
    # тот же updated, только с доступом по local id
    def identUpdated(self):
        for ext_id in self.updated:
            up = self.updated[ext_id]
            self.identified[up.id] = up


    # поиск перемещенных данных
    # заполняется self.moved
    def searchMoved(self):
        raise Exception('Implement requared!')


    # обновление родителей
    def setParents(self, parents):
        raise Exception('Implement requared!')


    # обработка изображений
    def procImage(self, src, adapt=False):
        # получаем путь до изображения
        path = urlparse(src).path[1:]

        # если изображения нет на диске
        if path not in self.context.originalImagesList:
            log.warning("procImage(): image '%s' was not loaded" % yell(path))
            return noImagePath

        originalRelPath = os.path.join(self.context.originalImagesDirRelPath, path)

        # если не нужно обрабатывать изображение
        if not adapt:
            log.info("procImage(): not adapt '%s'" % yell(path))
            return originalRelPath

        adaptedRelPath = os.path.join(self.context.adaptedImagesDirRelPath, path)

        # если еще не создавали обработанное изображение
        if path not in self.context.adaptedImagesList:
            log.info("procImage(): adapt '%s'" % yell(path))
            originalAbsPath = os.path.join(self.context.originalImagesDirAbsPath, path)
            adaptedAbsPath = os.path.join(self.context.adaptedImagesDirAbsPath, path)

            # пробуем создать каталоги, указанные в пути
            dname = os.path.dirname(adaptedAbsPath)
            try:
                os.makedirs(dname)
                log.info('procImage(): create dir %s' % yell(dname))
            except:
                pass


            goalwidth = 150
            goalheight = 150

            try:
                # пытаемся открыть исходное изображение
                img = Image.open(originalAbsPath)

                # ресайзим изображение
                currwidth, currheight = img.size
                ratio = goalwidth / currwidth if currwidth <= currheight else goalheight / currheight
                img.thumbnail((int(currwidth * ratio), int(currheight * ratio)), Image.BICUBIC)

                # обрезаем изображение
                currwidth, currheight = img.size
                left = int((currwidth - goalwidth) / 2)
                right = left + goalwidth
                img = img.crop((left, 0, right, goalheight))

                # сохраняем новое изображение
                img.save(adaptedAbsPath, 'JPEG')
            except IOError:
                log.error("procImage(): delete '%s'" % red(path))
                os.remove(originalAbsPath)
                return noImagePath

        return adaptedRelPath


    # красивая печаталка
    def __str__(self):
        return ('''
existing:  %s
updated:   %s
added:     %s
deleted:   %s
undeleted: %s
changed:   %s
moved:     %s
        ''' 
        % yell(
            len(self.existing), 
            len(self.updated), 
            len(self.added), 
            len(self.deleted), 
            len(self.undeleted),
            len(self.changed),
            len(self.moved)
        ))



# пост-обработка выкаченного контента
# класс должен переопределяться в реализациях
class BaseChanger:
    # обработка категорий
    def changeProductCategory(category):
        pass


    # обработка товаров
    def changeProduct(product):
        product.price = Decimal(product.price) * Decimal(1.15)


    # обработка вариаций
    def changeProductVariation(variation):
        variation.price = Decimal(variation.price) * Decimal(1.15)


    # обработка изображений
    def changeProductImage(image):
        pass