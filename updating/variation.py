# -*- coding: UTF-8 -*-

import logging
from decimal import Decimal
from lxml.html import clean
from updating.base import Elements
from updating.product import Products, procDescription
from common.base import blue, green, yell, red, mag, Element


log = logging.getLogger('variation')


class ProductVariations(Products):
    def __init__(self, context, connection, products):
        super(Products, self).__init__(context, connection)
        self.products = products


    # загрузка товаров из базы
    def loadExisting(self):
        getVariations = '''
            SELECT 
                id, ext_id, name,
                price, stock, preorder, limitation,
                rating, description, active, product_provider_id
            FROM products
            WHERE product_provider_id = %s AND product_id IS NOT NULL
        '''

        log.info('loadExisting(): started')
        self.cursor.execute(getVariations, [self.context.providerInfo.id])
        for data in self.cursor.fetchall():
            elem = Element(data)
            self.existing[elem.ext_id] = elem
        log.info('loadExisting(): finished')


    # обновление измененных товаров
    def updateChanged(self):
        updateProduct = '''
            UPDATE products 
            SET 
                name = %(name)s, 
                price = %(price)s, 
                stock = %(stock)s, 
                preorder = %(preorder)s,
                limitation = %(limitation)s,
                rating = %(rating)s, 
                description = %(description)s, 
                active = %(active)s,
                updated_at = now()

            WHERE id = %(id)s
        '''

        log.info('updateChanged(): started')
        for ext_id in self.changed:
            self.cursor.execute(updateProduct, self.updated[ext_id])
        log.info('updateChanged(): finished')


    # исключение товаров
    def excludeDeleted(self):
        excludeProduct = '''
            UPDATE products
            SET 
                active = FALSE,
                updated_at = now()
            WHERE id = %(id)s
        '''

        log.info('excludeDeleted(): started')
        for ext_id in self.deleted:
            self.cursor.execute(excludeProduct, self.existing[ext_id])
        log.info('excludeDeleted(): finished')


    # добавление новых товаров
    def addNew(self):
        addProduct = '''
            INSERT INTO products (
                name, ext_id, price, 
                preorder, limitation, stock, rating, 
                description, active, product_provider_id
            ) VALUES (
                %(name)s, %(ext_id)s, %(price)s, 
                %(preorder)s, %(limitation)s, %(stock)s, %(rating)s, 
                %(description)s, %(active)s, %(product_provider_id)s
            )
            RETURNING id
        '''

        log.info('addNew(): started')
        for ext_id in self.added:
            self.cursor.execute(addProduct, self.updated[ext_id])
            id = self.cursor.fetchone()[0]
            self.updated[ext_id].id = id
        log.info('addNew(): finished')


    def setParents(self):
        setParent = '''
            UPDATE products
            SET product_id = %s 
            WHERE id = %s
        '''

        log.info('setParents(): started')
        for ext_id in self.moved | self.added:
            up = self.updated[ext_id]
            self.cursor.execute(setParent, [self.products.updated[up.parent_id].id, up.id])