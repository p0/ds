# -*- coding: UTF-8 -*-

import logging
from updating.base import Elements
from common.base import blue, green, yell, red, mag, Element

log = logging.getLogger('image')


class ProductImages(Elements):
    def __init__(self, context, connection, products, productVariations):
        super().__init__(context, connection)
        self.products = products
        self.productVariations = productVariations

    # загрузка изображений из базы
    def loadExisting(self):
        getImages = '''
            SELECT 
                id, ext_id, active, image_src, 
                product_id, product_provider_id
            FROM product_images 
            WHERE product_provider_id = %s
        '''

        log.info('loadExisting(): started')
        self.cursor.execute(getImages, [self.context.providerInfo.id])
        for data in self.cursor.fetchall():
            elem = Element(data)
            self.existing[elem.ext_id] = elem
        log.info('loadExisting(): finished')


    # обработка изображений из обновления
    def procUpdated(self):
        log.info('procUpdated(): started')
        for image in self.updated.values():
            # не создаем обработанное изображение
            image.image_src = self.procImage(image.ext_id, adapt=False)
        log.info('procUpdated(): finished')


    # поиск измененных изображений 
    def searchChanged(self):
        attribs = ('active', 'image_src')
        super().searchChanged(attribs)


    # обновление измененных изображений
    def updateChanged(self):
        updateImage = '''
            UPDATE product_images 
            SET 
                active = %(active)s,
                image_src = %(image_src)s,
                updated_at = now()

            WHERE id = %(id)s
        '''

        log.info('updateChanged(): started')
        for ext_id in self.changed:
            self.cursor.execute(updateImage, self.updated[ext_id])
        log.info('updateChanged(): finished')


    # исключение изображений
    def excludeDeleted(self):
        excludeImage = '''
            UPDATE product_images
            SET 
                active = FALSE,
                updated_at = now()
            WHERE id = %(id)s
        '''

        log.info('excludeDeleted(): started')
        for ext_id in self.deleted:
            self.cursor.execute(excludeImage, self.existing[ext_id])
        log.info('excludeDeleted(): finished')


    # добавление новых изображений
    def addNew(self):
        addImage = '''
            INSERT INTO product_images (
                ext_id, active, product_provider_id, image_src
            ) VALUES (
                %(ext_id)s, %(active)s, %(product_provider_id)s, %(image_src)s
            )
            RETURNING id
        '''

        log.info('addNew(): started')
        for ext_id in self.added:
            self.cursor.execute(addImage, self.updated[ext_id])
            id = self.cursor.fetchone()[0]
            self.updated[ext_id].id = id
        log.info('addNew(): finished')


    # поиск перемещенных изображений
    def searchMoved(self):
        products = dict(list(self.products.identified.items()) + list(self.productVariations.identified.items()))
        
        log.info('searchMoved(): started')
        for ext_id in self.undeleted:
            up = self.updated[ext_id]
            ex = self.existing[ext_id]

            # если изменен родитель
            if up.parent_id != products[ex.product_id].ext_id:
                self.moved.add(ext_id)
        log.info('searchMoved(): finished')


    def setParents(self):
        setParent = '''
            UPDATE product_images 
            SET product_id = %s 
            WHERE id = %s
        '''

        products = dict(list(self.products.updated.items()) + list(self.productVariations.updated.items()))

        log.info('setParents(): started')
        for ext_id in self.moved | self.added:
            up = self.updated[ext_id]
            self.cursor.execute(setParent, [products[up.parent_id].id, up.id])

        log.info('setParents(): finished')