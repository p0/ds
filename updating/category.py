# -*- coding: UTF-8 -*-

import logging
from updating.base import Elements
from common.base import blue, green, yell, red, mag, Element

log = logging.getLogger('category')


__root__ = '__root__'

class ProductCategories(Elements):
    # загрузка категорий из базы
    def loadExisting(self):
        getCategories = '''
            SELECT 
                id, ext_id, name,
                position, active, product_category_id, product_provider_id
            FROM product_categories 
            WHERE product_provider_id = %s
        '''

        log.info('loadExisting(): started')
        self.cursor.execute(getCategories, [self.context.providerInfo.id])
        for data in self.cursor.fetchall():
            ex = Element(data)
            self.existing[ex.ext_id] = ex
            log.debug("loadExisting(): id: %s, ext_id: '%s'" % yell(ex.id, ex.ext_id))
        log.info('loadExisting(): finished')


    # обработка категорий из обновления
    def procUpdated(self):
        log.info('procUpdated(): started')
        for category in self.updated.values():
            # обработка корневых категорий
            if category.parent_id == '__root__':
                category.parent_id = None
        log.info('procUpdated(): finished')


    # поиск измененных категорий 
    def searchChanged(self):
        attribs = ('name', 'position', 'active')
        super().searchChanged(attribs)


    # обновление измененных категорий
    def updateChanged(self):
        updateCategory = '''
            UPDATE product_categories 
            SET 
                name = %(name)s, 
                position = %(position)s,
                active = %(active)s,
                updated_at = now()
            WHERE id = %(id)s
        '''

        log.info('updateChanged(): started')
        for ext_id in self.changed:
            self.cursor.execute(updateCategory, self.updated[ext_id])
        log.info('updateChanged(): finished')


    # исключение удаленных категорий
    def excludeDeleted(self):
        excludeCategory = '''
            UPDATE product_categories
            SET 
                active = FALSE,
                updated_at = now()
            WHERE id = %(id)s
        '''

        log.info('excludeDeleted(): started')
        for ext_id in self.deleted:
            self.cursor.execute(excludeCategory, self.existing[ext_id])
        log.info('excludeDeleted(): finished')


    # добавление новых категорий
    def addNew(self):
        addProduct = '''
            INSERT INTO product_categories (
                name, ext_id, position, 
                active, product_provider_id
            ) VALUES (
                %(name)s, %(ext_id)s, %(position)s,
                %(active)s, %(product_provider_id)s
            )
            RETURNING id
        '''

        log.info('addNew(): started')
        for ext_id in self.added:
            self.cursor.execute(addProduct, self.updated[ext_id])
            self.updated[ext_id].id = self.cursor.fetchone()[0]
        log.info('addNew(): finished')


    # поиск перемещенных данных
    def searchMoved(self):
        log.info('searchMoved(): started')
        for ext_id in self.undeleted:
            up = self.updated[ext_id]
            ex = self.existing[ext_id]

            # если изменен родитель
            if up.parent_id != (None if ex.product_category_id is None else self.identified[ex.product_category_id].ext_id):
                log.debug("searchMoved(): %s -> %s" % yell(up.parent_id, self.identified[ex.product_category_id].ext_id))
                self.moved.add(ext_id)
        log.info('searchMoved(): finished')


    # родители категорий
    def setParents(self):
        setParent = '''
            UPDATE product_categories
            SET product_category_id = %(product_category_id)s
            WHERE id = %(id)s
        '''

        log.info('setParents(): started')
        for ext_id in self.moved | self.added:
            up = self.updated[ext_id]
            up.product_category_id = None if up.parent_id is None else self.updated[up.parent_id].id
            self.cursor.execute(setParent, up)
        log.info('setParents(): finished')