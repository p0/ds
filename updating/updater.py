#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import signal
import traceback
import time
import json
import psycopg2
import psycopg2.extras
import logging
from datetime import datetime
from collections import OrderedDict
from lxml import etree, objectify
from common.base import main, startLogging, cutString, blue, green, yell, red, mag, Element, checkProviderExist, makeDirs
from common.config import PG_USER, PG_DB, IMAGES_PATH, UPDATING_PATH
from updating.category import ProductCategories
from updating.product import Products
from updating.variation import ProductVariations
from updating.image import ProductImages 



class Context:
    def __init__(self, providerInfo):
        self.providerInfo = providerInfo

        # каталоги с исходными и обработанными изображениями
        self.originalImagesDirRelPath = os.path.join('images', providerInfo.name, 'original')
        self.adaptedImagesDirRelPath = os.path.join('images', providerInfo.name, 'adapted')
        self.originalImagesDirAbsPath = os.path.join(IMAGES_PATH, providerInfo.name, 'original')
        self.adaptedImagesDirAbsPath = os.path.join(IMAGES_PATH, providerInfo.name, 'adapted')

        # список исходных изображений
        self.originalImagesList = []
        for root, dirs, files in os.walk(self.originalImagesDirAbsPath):
            for f in files:
                self.originalImagesList.append(os.path.join(os.path.basename(root), f))

        # список обработанных изображений
        self.adaptedImagesList = []
        for root, dirs, files in os.walk(self.adaptedImagesDirAbsPath):
            for f in files:
                self.adaptedImagesList.append(os.path.join(os.path.basename(root), f))


class Updater:
    def __init__(self, source, providerName, Changer):
        # исходные данные
        self.source = source

        # инфа о поставщике
        providerInfo = getProviderInfo(providerName)

        # создаем контекст
        self.context = Context(providerInfo)

        self.productCategories = ProductCategories(self.context, self.connection)
        self.products = Products(self.context, self.connection, self.productCategories)
        self.productVariations = ProductVariations(self.context, self.connection, self.products)
        self.productImages = ProductImages(self.context, self.connection, self.products, self.productVariations)

        self.groups = OrderedDict()
        self.groups['category'] = self.productCategories
        self.groups['product'] = self.products
        self.groups['variation'] = self.productVariations
        self.groups['image'] = self.productImages

        self.changers = {}
        self.changers['category'] = Changer.changeProductCategory
        self.changers['product'] = Changer.changeProduct
        self.changers['variation'] = Changer.changeProductVariation
        self.changers['image'] = Changer.changeProductImage


    # загрузка данных из обновления
    def loadUpdate(self):
        log.info('loadUpdate(): started')
        for data in self.source:
            el = Element(data)

            # раскладываем по типу
            elements = self.groups[el.type].updated
            changer = self.changers[el.type]

            # проверяем, что идентификатор уникален
            if el.ext_id in elements:
                log.warning("loadUpdate(): %s %s already was added!" % (el.type, yell(el.ext_id)))
            elements[el.ext_id] = el

            # указываем поставщика
            el.product_provider_id = self.context.providerInfo.id
            
            # по умолчанию считаем элемент активным
            el.active = bool(el.active) if 'active' in el else True

            # производим пост-обработку данных
            changer(el)

        log.info('loadUpdate(): finished')


    # обновление
    def update(self):
        for group in self.groups.values():
            # загрузка данных из базы
            group.loadExisting()
            # предварительная обработка данных обновления
            group.procUpdated()
            # поиск добавленных, измененных и оставленных данных
            group.fillSets()
            # поиск измененных данных
            group.searchChanged()
            # обновление измененных данных
            group.updateChanged()
            # исключение отутсвующих в обновлении данных
            group.excludeDeleted()
            # добавление новых данных
            group.addNew()
            # идентификация данных обновления
            group.identUpdated()
            # поиск перемещенных данных
            group.searchMoved()
            # обновление родителей
            group.setParents()

        for name, group in self.groups.items():
            log.info('%s: %s' % (name, group))

        self.connection.commit()
        self.connection.close()



if __name__ == '__main__':
    # проверяем, что переданы все параметры
    if len(sys.argv) < 3:
        print('Error: expected provider name and source file!\nUsage: %s <provider_name> <source_file>' % sys.argv[0])
        sys.exit(1)

    # проверяем, что поставщик существует
    providerName = sys.argv[1]
    if not checkProviderExist(providerName):
        sys.exit(1)

    # проверяем, что файл с исходными данными существует
    if not os.path.isfile(sys.argv[2]):
        print('Error: unexists source file %s' % sys.argv[2])
        sys.exit(1)

    # создаем необходимые каталоги, если их нету
    logdir = os.path.join(UPDATING_PATH, 'logs')
    adpdir = os.path.join(IMAGES_PATH, providerName, 'adapted')
    makeDirs(logdir, adpdir)

    # начинаем логгирование
    startLogging(os.path.join(logdir, providerName))
    log = logging.getLogger('updater')

    # импортим реализацию пост-обработчика
    exec('from providers.%s.changer import Changer' % providerName)

    log.info('update(): %s' % green('*** UPDATING STARTED ***'))
    sourceFile = open(sys.argv[2])
    log.info('update(): load %s MiB from %s' % yell(float(os.path.getsize(sys.argv[2])) / 1048576, sys.argv[2]))
    source = json.loads(sourceFile.read())
    updater = Updater(source, providerName, Changer)
    startTime = datetime.now()
    updater.loadUpdate()
    updater.update()
    finishTime = datetime.now()
    log.info('update():\nstarted: %s\nfinished: %s\nperfomed: %s\n' % yell(startTime, finishTime, finishTime - startTime))
    log.info('update(): %s' % green('*** UPDATING COMPLETE ***'))