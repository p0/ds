import os
import logging
import traceback
import json
import psycopg2
import pickle
import redis
import time
from datetime import datetime
from common.base import blue, green, yell, red
from common.browser import Browser, skipUrl
from common.config import LOADING_PATH, IMAGES_PATH, PG_USER, PG_DB, REDIS_HOST, REDIS_PORT, REDIS_DB


log = logging.getLogger('tasks')


# конфигурация loader'a
class LoaderConfig():
    processQnt = 3                  # количество процессов-loader'ов
    attemptQnt = 5                  # количество попыток выполнения задачи
    pauseMs = 500                   # пауза перед выполнением следующей задачи
    verifyUrl = skipUrl             # проверочный адрес
    verifyJs = '1'                  # проверочный js
    mode = '__unknown__'            # режим загрузки



# абстрактная задача, с которой работает loader
# tid - идентификатор задачи. Удобно брать url в качестве tid
# parent_id - родитель задачи
# allowFail - не убивать loader после attemptQnt запоротых попыток
class BaseTask():
    def __init__(self, tid, parent_id=None, allowFail=False):
        self.tid = tid
        self.parent_id = parent_id
        self.allowFail = allowFail

        # помечаем, что не выполняли эту задачу
        self.attempt = 0


    # процесс решения задачи, переопределяется в конкретных реализациях
    # должен возвращать (childs, result)
    #   childs - список дочерних задач. Может быть пустым
    #   result - результат выполнения задачи в виде питоновского списка или словаря
    def execute(self):
        raise Exception('This method is not implemented')


    def __str__(self):
        return '%s( %s )' % yell(self.__class__.__name__, self.tid)



# задача для загрузки изображения
# универсальная для всех магазинов
# забиваем, если не смогли выкачать изображение
class LoadImage(BaseTask):
    def __init__(self, tid, providerName, allowFail=True):
        super().__init__(tid, allowFail=allowFail)

        # сохраняем изображения в подкаталог original
        self.path = os.path.join(IMAGES_PATH, providerName, 'original')


    def execute(self):
        log.info('Load image %s' % yell(self.tid))
        # отключаем подгрузку jQuery и увеличиваем таймаут
        browser = Browser(autoJquerify=False, timeoutMs=120000)
        browser.load(self.tid, self.path)
        del browser
        return ([], [])


class Tasks:
    def __init__(self, Config, providerInfo):
        log.info('__init__(): connect to redis db %s on %s:%s' % yell(REDIS_DB, REDIS_HOST, REDIS_PORT))
        self.wcon = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

        self.Config = Config
        self.providerInfo = providerInfo
        providerId = providerInfo.id
        mode = Config.mode
        self.proxies = 'loader:%s:%s:proxies' % (providerId, mode)
        self.queue = 'loader:%s:%s:queue' % (providerId, mode)
        self.results = 'loader:%s:%s:results' % (providerId, mode)
        self.put = 'loader:%s:%s:put' % (providerId, mode)
        self.done = 'loader:%s:%s:done' % (providerId, mode)

        
    # чистим инфу с прошлых засосов
    def clearData(self):
        self.wcon.delete(self.proxies)
        self.wcon.delete(self.queue)
        self.wcon.delete(self.results)
        self.wcon.delete(self.put)
        self.wcon.delete(self.done)


    # загрузка проксей из базы
    def loadProxies(self):
        con = psycopg2.connect('dbname=%s user=%s' % (PG_DB, PG_USER))
        cur = con.cursor()
        cur.execute("SELECT ip, port FROM proxies WHERE protocol IN ('HTTP', 'HTTPS')")
        log.info('loadProxies(): %s' % str(cur.query, 'utf-8'))
        log.info('loadProxies(): %s' % cur.statusmessage)
        self.wcon.sadd(self.proxies, *['%s:%s' % proxy for proxy in cur])
        cur.close()
        con.close()


    # выбор прокси
    def choiceProxy(self):
        proxy = str(self.wcon.srandmember(self.proxies), 'utf-8').split(':')
        log.info('choiceProxy(): %s:%s' % yell(proxy))
        return proxy


    # удаление прокси
    def removeProxy(self, host, port):
        self.wcon.srem(self.proxies, '%s:%s' % (host, port))


    # положить задачу в очередь
    def putTask(self, task):
        log.info('putTask(): %s' % task)
        self.wcon.rpush(self.queue, pickle.dumps(task))
        self.wcon.sadd(self.put, task.tid)


    # вытащить задачу из очереди
    def popTask(self):
        task = pickle.loads(self.wcon.blpop(self.queue)[1])
        log.info('popTask(): %s' % task)
        return task


    # сохранение результата выполнения задачи
    def saveResult(self, result, task):
        # если это список, то распаковываем его, иначе суем просто так
        if isinstance(result, (list, tuple)):
            for item in result:
                self.wcon.rpush(self.results, pickle.dumps(item))
        else:
            self.wcon.rpush(self.results, pickle.dumps(result))

        log.info('saveResult(): %s' % task)
        self.wcon.sadd(self.done, task.tid)


    # уже работаем над этой задачей?
    def checkTask(self, task):
        return self.wcon.sismember(self.put, task.tid)        


    # сохранение списка results в файл
    def saveToFile(self, normalExit=True):
        resname = os.path.join(LOADING_PATH, 'results' if normalExit else 'fails', self.providerInfo.name, '%s.json' % datetime.now().isoformat())
        results = [pickle.loads(result) for result in self.wcon.lrange(self.results, 0, self.wcon.llen(self.results))]
        save = json.dumps(results, indent=2, separators=(',', ': '))
        log.info('saveToFile(): save results to %s' % yell(resname))
        with open(resname, 'w') as f:
            f.write(save)


    # ожидание завершения загрузки
    def waitDone(self):
        while self.wcon.sdiff(self.put, self.done):
            time.sleep(1)


    # текущее состояние загрузки
    def currentState(self):
        queueSize = self.wcon.llen(self.queue)
        putSize = self.wcon.scard(self.put)
        doneSize = self.wcon.scard(self.done)
        resultsSize = self.wcon.llen(self.results)
        proxiesSize = self.wcon.scard(self.proxies)
        return queueSize, putSize, doneSize, resultsSize, proxiesSize