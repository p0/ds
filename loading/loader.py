#!/usr/bin/env python

import os
import sys
import signal
import traceback
import time
import json
import logging
import resource
from datetime import datetime, timedelta
from multiprocessing import Process
from threading import Thread, Event
from PyQt4.QtNetwork import QNetworkProxy
from tasks import Tasks
from common.base import main, startLogging, blue, green, yell, red, checkProviderExist, getProviderInfo, makeDirs
from common.config import PROVIDERS_PATH, LOADING_PATH, IMAGES_PATH, PG_USER, PG_DB
from common.browser import Browser, WebkitError

log = logging.getLogger('loader')


# мониторинг
class Monitor(Thread):
    def __init__(self, intervalSec):
        super().__init__()
        self.intervalSec = intervalSec
        self.stopEvent = Event()


    # завершение мониторинга
    def stop(self):
        self.stopEvent.set()


    def run(self):
        # запоминаем время старта
        startTime = time.time()

        while not self.stopEvent.is_set():
            # текущее время
            currentTime = time.time()
            uptime = str(timedelta(seconds=int(currentTime - startTime)))

            # использование памяти
            memory = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
            memory += resource.getrusage(resource.RUSAGE_CHILDREN).ru_maxrss
            memory /= 1024

            # логгируем текущее состояние
            queueSize, putSize, doneSize, resultsSize, proxiesSize = tasks.currentState()
            state = (uptime, int(memory), queueSize, putSize, doneSize, resultsSize, proxiesSize)
            log.info('manage(): uptime: %s, mem: %s MiB, tasks: %s, put: %s, done: %s, res: %s, prox: %s' % blue(state))

            # ожидаем сигнала остановки или истечения таймаута
            self.stopEvent.wait(self.intervalSec)



# аккуратное завершение работы
def stopping(normalExit):
    # оcтановка loader'ов
    for proc in processes:
        log.info('stopping(): stop process %s' % blue(proc.pid))
        proc.terminate()
        proc.join()

    # остановка мониторинга
    monitor.stop()
    monitor.join()

    # сохраняем скаченное
    tasks.saveToFile(normalExit)

    # чистим redis
    tasks.clearData()



# процесс-загрузчик
def load(Config, providerInfo):
    log = logging.getLogger('ldrproc')
    tasks = Tasks(Config, providerInfo)

    @main
    def mainloop():
        while True:
            try:
                # берем из очереди задачу
                task = tasks.popTask()

                if Config.mode != 'test':
                    # выбираем для нее рандомный прокси
                    host, port = tasks.choiceProxy()
                    proxy = QNetworkProxy(QNetworkProxy.HttpProxy, host, int(port))
                    QNetworkProxy.setApplicationProxy(proxy)

                # и пытаемся ее выполнить
                log.info('load(): start execute %s' % task)
                result, childs = task.execute()
                log.info('load(): finish execute %s' % task)

                # бежим по детям
                for child in childs:
                    # кладем задачу в очередь, если еще не рассматривали ее
                    # в режиме noimg не кладем задачи LoadImage
                    if tasks.checkTask(child) or (Config.mode == 'noimg' and child.__class__.__name__ == 'LoadImage'):
                        log.info('load(): skip task %s' % child)
                    else:
                        tasks.putTask(child)

                # сохраняем результат
                tasks.saveResult(result, task)

                # спим
                log.info('load(): sleep %s msec' % yell(Config.pauseMs))  
                time.sleep(float(Config.pauseMs) / 1000)

            # ловим ошибки браузеров
            except WebkitError:
                log.error('load(): %s attempt(s) of %s is failed\n\n%s' % (red(task.attempt + 1), task, red(traceback.format_exc())))

                # может быть лагает прокси
                try:
                    if Config.mode != 'test':
                        log.info('load(): check proxy %s:%s' % yell(host, port))
                        browser = Browser()
                        # даю несколько раз сходить на проверочный адрес и выполнить там проверочный js
                        browser.get(Config.verifyUrl, Config.verifyJs, attempts=3)
                        del browser

                        # прокси скорее жив чем мертв
                        log.info('load(): proxy %s:%s is normal' % green(host, port))

                    # если использовали не все попытки, 
                    # кладем задачу обратно в очередь
                    # и увеличиваем номер попытки
                    if task.attempt < Config.attemptQnt:
                        task.attempt += 1
                        tasks.putTask(task)
                        log.info('load(): %s attempt to give for %s' % (yell(task.attempt + 1), task))

                    # если все попытки исчерпаны,
                    else:
                        if task.allowFail:
                            # похеру, что запороли attemptQnt попыток
                            log.warning('load(): all attempts of %s is failed but no matter' % task)
                        else:
                            # шлем управляющему процессу сигнал, что у нас тут кровь-кишки
                            # управляющий процесс в ответ должен загасить всех loader'ов
                            log.error('load(): %s attempts of %s is failed' % (red('ALL'), task.tid))
                            os.kill(os.getppid(), signal.SIGINT)

                except WebkitError:
                    # удалим мертвый прокси из списка
                    tasks.removeProxy(host, port)

                    # задачу опять бросим в очередь
                    tasks.putTask(task)
                    log.info('load(): %s attempt to give for %s (not changed)' % yell(task.attempt + 1, task.tid))

            except:
                # все плохо, просим управляющий процесс прибить loader'ов
                log.error('load():\n\n%s' % red(traceback.format_exc()))
                os.kill(os.getppid(), signal.SIGINT)

    # стартуем loader
    mainloop()



# управляющий процесс
# создает и запускает loader'ы
# потом ждет, пока они отработают, либо в одном из loader'ов не возникнет ошибка
# если все выкачивание прошло успешно, сохраняем результат в файл
if __name__ == '__main__':
    # проверяем, что переданы все параметры
    if len(sys.argv) < 3:
        print('Error: expected provider name and loader mode!\nUsage : %s <provider_name> all|noimg|test' % sys.argv[0])
        sys.exit(1)

    # проверяем, что поставщик существует
    providerName = sys.argv[1]
    if not checkProviderExist(providerName):
        sys.exit(1)

    # проверяем режим
    # all - качаем все
    # noimg - качаем все кроме картинок
    # test - отладка (качает один процесс-loader без прокси)
    mode = sys.argv[2]
    allowModes = ('all', 'noimg', 'test')
    if mode not in allowModes:
        print('Error: unknown mode\nUse: %s' % ', '.join(allowModes))
        sys.exit(1)

    # создаем  необходимые каталоги, если их нету
    logdir = os.path.join(LOADING_PATH, 'logs')
    resdir = os.path.join(LOADING_PATH, 'results', providerName)
    faildir = os.path.join(LOADING_PATH, 'fails', providerName)
    imgdir = os.path.join(IMAGES_PATH, providerName, 'original')
    makeDirs(logdir, resdir, faildir, imgdir)

    # начинаем логгирование
    startLogging(os.path.join(logdir, providerName))
    log = logging.getLogger('mngproc')
    log.info('manage(): %s' % green('*** LOADING STARTED - %s MODE ***' % mode.upper()))
    log.info('manage(): start manage process %s' % blue(os.getpid()))

    # импортим из реализации настройки и корневую задачу
    # в тестовом режиме корневая задача называется 'test'
    exec('from providers.%s.tasks import Config, %s as root' % (providerName, 'test' if mode == 'test' else 'root'))

    # прописываем режим в конфиге, что бы из реализации его можно было узнать
    Config.mode = mode

    processes = []          # пул процессов loader'ов
    monitor = Monitor(60)   # мониторинг в отдельном потоке

    providerInfo = getProviderInfo(providerName)

    # на всякий случай чистим redis после предыдущих засосов
    tasks = Tasks(Config, providerInfo)
    tasks.clearData()

    # в тестовом режиме загружаем в 1 поток и не грузим прокси
    if mode == 'test':
        Config.processQnt = 1
    else: 
        tasks.loadProxies()

    # кладем в очередь корневую задачу
    tasks.putTask(root)
    
    # стартуем loader'ы
    for i in range(Config.processQnt):
        proc = Process(target=load, args=(Config, providerInfo))
        processes.append(proc)
        proc.daemon = True
        proc.start()
        log.info('manage(): start loader process %s' % blue(proc.pid))

    # начинаем мониторинг в отдельном потоке
    monitor.start()

    # ждем завершения работы loader'ов
    # ловим сигнал остановки
    # может прийти от нас, если мы нажмем ^c, либо от одного из loader'ов в случае ошибки
    try:
        tasks.waitDone()
    except KeyboardInterrupt:
        # гасим все loader'ы
        log.error('manage(): %s' % red('receive interrupt signal'))
        stopping(normalExit=False)
        log.error('manage(): %s' % red('*** LOADING INCOMPLETE ***'))

        # потом суицид
        sys.exit(2)

    # гасим все loader'ы
    stopping(normalExit=True)
    log.info('manage(): %s' % green('*** LOADING COMPLETE ***'))