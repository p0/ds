#!/usr/bin/env python

import logging
import psycopg2
import psycopg2.extras
import redis
from datetime import date
from common.browser import Browser, WebkitError
from common.base import main, startLogging, blue, green, yell, red, mag
from common.config import PG_USER, PG_DB, REDIS_HOST, REDIS_PORT, REDIS_DB

startLogging('currency_cources_loader')
log = logging.getLogger('hs')

@main
def load():

    # идем на сайт ЦБ РФ
    browser = Browser()
    browser.get('http://www.cbr.ru/currency_base/daily.aspx?date_req=%s' % date.today().strftime('%d.%m.%Y'), "$('table.CBRTBL').length")

    parseCurrencyCourses = '''
        var currencyCourses = {}
        $('table.CBRTBL tr:gt(0)').each(function(i) {
            var code = $('td:eq(1)', this).text().slice(-3).toLowerCase()
            var course = parseFloat($('td:eq(4)', this).text()) / parseInt($('td:eq(2)', this).text())
            currencyCourses[code] = course
        })
        JSON.stringify(currencyCourses)
    '''

    # парсим курсы валют
    currencyCourses = browser.parseToJson(parseCurrencyCourses)

    # складываем их в базу
    log.info('load(): connect to database: %s, user: %s' % yell(PG_DB, PG_USER))
    con = psycopg2.connect('dbname=%s user=%s' % (PG_DB, PG_USER))
    cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cur.execute("INSERT INTO currency_courses (%s) VALUES (%s)" % (', '.join(currencyCourses.keys()), ', '.join(map(str, currencyCourses.values()))))
    
    # достаем из базы инфу о валюте поставщиков
    cur.execute("SELECT id, currency FROM product_providers")
    providers = cur.fetchall()

    cur.close()
    con.commit()
    con.close()

    # заносим в redis
    con = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

    for provider in providers:
        con.hmset('provider:%d' % provider['id'], 
            {'currency': provider['currency'], 'currency_course': currencyCourses[provider['currency']]})


load()
