#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import psycopg2
import logging
from common.base import main, startLogging, blue, green, yell, red
from common.config import PG_USER, PG_DB
from common.browser import Browser, WebkitError


startLogging('proxy_list_loader')
log = logging.getLogger('load')

@main
def loadProxyList():
    browser = Browser()
    browser.get('http://spys.ru/proxies/', "$('tr.spy1xx').length")

    parseProxyList = '''
        $('script').remove()
        var root = []

        $('.spy1xx:gt(0), .spy1x:gt(0)').each(function(i) {
            var ip_port = $('td:eq(0) font.spy14', this).text().split(':')
            var protocol = $('td:eq(1)', this).text()
            var anonymity = $('td:eq(2)', this).text()
            country = $('td:eq(4)', this).text().slice(0, 2)

            var proxy = {
                ip: ip_port[0],
                port: ip_port[1],
                protocol: protocol,
                anonymity: anonymity,
                country: country
            }

            root.push(proxy)
        })
        JSON.stringify(root)
    '''

    parsePaginator = '''
        var root = []
        $('tr.spy1xx:eq(0) a:gt(0)').each(function(i) {
            root.push(this.href)
        })
        JSON.stringify(root)
    '''

    log.info('Loading started')
    proxies = browser.parseToJson(parseProxyList)
    urls = browser.parseToJson(parsePaginator)
    
    for url in urls:
        browser.sleep(5)
        browser.get(url, "$('tr.spy1xx').length")
        proxies += browser.parseToJson(parseProxyList)

    con = psycopg2.connect('dbname=%s user=%s' % (PG_DB, PG_USER))
    cur = con.cursor()

    sql = '''
        DROP TABLE IF EXISTS proxies;

        CREATE TABLE proxies (
            id          serial          PRIMARY KEY,
            ip          varchar(15)     NOT NULL,
            port        varchar(6)      NOT NULL,
            anonymity   char(3)         NOT NULL,
            protocol    varchar(10)     NOT NULL,
            country     char(2)         NOT NULL
        );
    '''
    cur.execute(sql)
    log.info(str(cur.query, 'utf-8').strip())
    log.info(cur.statusmessage)

    sql = '''
        INSERT INTO proxies (ip, port, protocol, anonymity, country) 
        VALUES (%(ip)s, %(port)s, %(protocol)s, %(anonymity)s, %(country)s)
    '''
    for proxy in proxies:
        log.debug(str(proxy))
        cur.execute(sql, proxy)
        log.info(str(cur.query, 'utf-8').strip())
        log.info(cur.statusmessage)

    con.commit()
    cur.close()
    con.close()

if __name__ == '__main__':
    loadProxyList()