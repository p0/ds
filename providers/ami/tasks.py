# -*- coding: utf-8 -*-

import logging
from common.base import red, yell
from common.browser import Browser, WebkitError
from loading.loader import BaseTask, LoaderConfig, File
from updating.category import __root__


log = logging.getLogger('ami')


class AmiConfig(ShopConfig, LoaderConfig):
    processQnt = 3
    attemptQnt = 5
    pauseMs = 500       #это задержка между запросами к сайту
    verifyUrl = 'http://www.amiami.com/'
    verifyJs = "$('p.top_genre').length"


class RootCategories(BaseTask):
    def execute(self):
        log.debug('execute(): parse categories')
        browser = Browser()
        browser.get(self.tid, "$('p.top_genre').length")
        

        
        parseCategories = '''
            var root = []
            var i = 0
            $('li.genre_list:gt(0)').each(function(i) {
                var ext_id =  /(.+\/)(.+)(\..+)/.exec($('a', this).attr('href'))[2]        
                var name = $(this).text()
                var category = {type: "category", ext_id: ext_id, name: name, position: i++}
                root.push(category)
            })
            JSON.stringify(root)
        '''

        categories = browser.parseToJson(parseCategories)
        del browser

        childs = []
        for category in categories:
            category['parent_id'] = __root__
            child = ProductList('http://www.amiami.com/top/page/c/%s.html' % category['ext_id'], category['ext_id'])    
            childs.append(child)
        return (categories, childs)


class ProductList(BaseTask):
    def execute(self):
        log.debug('execute(): parse product list')
        browser = Browser()
        browser.get(self.tid, "$('h3.heading_07').length")


        parseSmallImages = '''
            // картинки
            images = []
           
            
             $('div.product_img a').each(function(i) {
                // поля src теперь нет, см спеки
                var ext_id = $('img', this).attr('src')
                var parent_id = /(.+code=)(.+)(&.+)/.exec($(this).attr('href'))[2]

                if (ext_id == "http://img.amiami.jp/images/product/thumbnail/noimage.gif")
                    ext_id = "__noimg__"

                var file = {type: "image", ext_id: ext_id, parent_id: parent_id, attr: "small"} // ну или как назвали этот аттрибут
                images.push(file)
            })

            JSON.stringify(images)
        '''

        imageList = browser.parseToJson(parseSmallImages)
        del browser

        childs = []
        for image in imageList:
            child = Product('http://www.amiami.com/top/detail/detail?gcode=%s&page=top' % image['parent_id'], self.parent_id)
            childs.append(child)
        return (imageList , childs)



class Product(BaseTask):
    def execute(self):
        log.debug('execute(): parse product')
        browser = Browser()
        browser.get(self.tid, "$('p.product_img_area').length")

        parseProduct = '''
            // выковыриваем со страницы товара

            // их идентификатор
            var ext_id = /(.+code=)(.+)(&.+)/.exec(window.location.href)[2]

            // рейтинг
            rating = '2.5'

            // название товара 
            var name = $('h2.heading_10').text().trim()

            // цену в правильной валюте 
            $('span.off_price').remove()
            var price = /(.+)(\s)/.exec($('li.price').text())[1].replace(',', '.')           
            
            // количество на складе
            var stock = '+'

            // возможность предзаказа     разобраться!
            var backorder = $('li.selling_price').text()

            // описание
            var description = $('p.box_01:eq(1)').html()

            //Максимальное количество на 1 семью
            //limit = $('dl.spec_data b').text()

            //Дата релиза
            //release_date = /(early|late|mid)(.+09|10|11|12|13|14|15|16|17)/.exec($('dl.spec_data dd').text())[0]

            var product = {
                type: "product",
                ext_id: ext_id,
                name: name,
                price: price,
                rating: rating,
                stock: stock,
                backorder: backorder,
                description: description,
            }
            
            JSON.stringify(product)
        '''

        parseImages = '''
            // картинки
            files = []
           
            
            $('p.product_img_area img').each(function(i) {
                var src = $(this).attr('src')
                var ext_id = /.+\/(.+)\.jpg/.exec(src)[1]
                var file = {type: "image", src: src, ext_id: ext_id}
                files.push(file)
            })


            $('div.product_img_area a').each(function(i) {
                var src = $(this).attr('href')
                var ext_id = /.+\/(.+)\.jpg/.exec(src)[1]
                var file = {type: "image", src: src, ext_id: ext_id}
                files.push(file)
            })

            // помечаем основное изображение
            if (files.length)
                files[0].attr = 'main'


            // для отладки
            /*
            if (files.length)
                files[0].attr = 'main'
            for (var i = 0; i <= files.length; i++) {
                console.log(files[i]);
            }
            console.log(files.length);
            */
            JSON.stringify(files)
        '''




        product = browser.parseToJson(parseProduct)
        files = browser.parseToJson(parseImages)
        del browser

        product['parent'] = self.parent_id
        ext_id = product['ext_id']

        childs = []
        for file in files:
            file['parent'] = ext_id
            child = File(file['src'], Config.providerName)
            childs.append(child)

        files.append(product)
        return (files, childs)


root = RootCategories('http://www.amiami.com/')
test = ProductList('http://www.amiami.com/top/page/c/bishoujo.html', 'ololo')