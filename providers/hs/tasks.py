import logging
from common.base import red, yell
from common.browser import Browser, WebkitError
from loading.tasks import BaseTask, LoaderConfig, LoadImage
from updating.category import __root__


log = logging.getLogger('hs')


class Config(LoaderConfig):
    processQnt = 7
    attemptQnt = 5
    pauseMs = 500
    verifyUrl = 'http://www.1999.co.jp/eng/'
    verifyJs = "$('div.hd_cart').length"


class RootCategories(BaseTask):
    def execute(self):
        log.debug('execute(): parse categories')
        browser = Browser()
        browser.get(self.tid, "$('div.hd_cart').length")

        parseCategories = '''
            var root = []
            var i = 0
            $('ul.left_list:eq(0) li').each(function(i) {
                var ext_id = /\d+/.exec($('a', this).attr('href'))[0]
                var name = $(this).text()
                var category = {type: "category", ext_id: ext_id, name: name, position: i++}
                root.push(category)
            })
            JSON.stringify(root)
        '''

        categories = browser.parseToJson(parseCategories)
        del browser

        childs = []
        for category in categories:
            category['parent_id'] = __root__
            child = ProductList('http://www.1999.co.jp/eng/list/%s/0/1' % category['ext_id'], category['ext_id'])
            childs.append(child)
        return (categories, childs)


class ProductList(BaseTask):
    def execute(self):
        log.debug('execute(): parse product list')
        browser = Browser()
        browser.get(self.tid, "$('table.list9pt').length")

        parseProductList = '''
            var root = []
            
            var link = $('div.right > table:eq(0) td:eq(2) a:contains(Next)').attr('href')
            var next = {type: "next", link: link}
            root.push(next)

            $('div.right > table').each(function(i) {
                if ( ! $('form', this).length )
                    return

                var ext_id = $('input[name=It_c]', this).attr('value')
                var product = {type: "product", ext_id: ext_id}
                root.push(product)
            })
            
            JSON.stringify(root)
        '''

        productList = browser.parseToJson(parseProductList)
        del browser

        childs = []
        next = productList[0].get('link', None)
        if next:
            child = ProductList('http://www.1999.co.jp%s' % next, self.parent_id)
            childs.append(child)

        for product in productList[1:]:
            ext_id = product.get('ext_id', None)
            if not ext_id:
                raise WebkitError("Field 'ext_id' not found")
            child = Product('http://www.1999.co.jp/eng/%s' % product['ext_id'], self.parent_id)
            childs.append(child)
        return ([], childs)


class Product(BaseTask):
    def execute(self):
        log.debug('execute(): parse product')
        browser = Browser()
        browser.get(self.tid, "$('div.bikou').length")

        parseProduct = '''
            // выковыриваем со страницы товара

            // ихний идентификатор
            var ext_id = /\d+$/.exec(window.location.href)[0]

            // название товара
            var name = $('div.right table:eq(0) td.item_name1').text().trim()

            // цену в правильной валюте
            var price = /[\d,.]+/.exec( $('table.xxx15 tr:contains(Regular Price) span.txtnormal').text() )[0].replace(',', '')

            // рейтинг у них на сайте
            var rating = $('div.recommend:eq(0) b').text()
            if (! rating)
                rating = '0'

            // количество на складе
            var stock = '-'

            if ($('input.cart_button[value="Cart"]').length)
                stock = '+'

            // возможность предзаказа
            var preorder = '0'

            // описание
            $('div.bikou div.txt9pt').remove()
            var description = $('div.bikou').html()

            // основное изображение
            var image_ext_id = $('td[width=120]:last() img').attr('src')

            var product = {
                type: "product",
                ext_id: ext_id,
                name: name,
                price: price,
                rating: rating,
                stock: stock,
                preorder: preorder,
                image_ext_id: image_ext_id,
                description: description,
            }

            JSON.stringify(product)
        '''

        parseImages = '''
            // картинки
            images = []
            $('img[src*=itbig]').each(function(i) {
                var ext_id = this.src.replace('_s', '')
                var image = {type: "image", ext_id: ext_id}
                images.push(image)
            })

            JSON.stringify(images)
        '''

        product = browser.parseToJson(parseProduct)
        images = browser.parseToJson(parseImages)
        del browser

        product['parent_id'] = self.parent_id
        ext_id = product['ext_id']

        childs = []
        for image in images:
            image['parent_id'] = ext_id
            child = LoadImage(image['ext_id'], Config.providerName)
            childs.append(child)

        images.append(product)
        return (images, childs)


tests = dict()
tests['product'] = Product('http://www.1999.co.jp/eng/10212484', '000')
tests['image'] = LoadImage('http://www.1999.co.jp/itbig11/10115166a4.jpg', 'hs')
test = tests['product']

root = RootCategories('http://www.1999.co.jp/eng/')