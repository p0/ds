import logging
from PyQt4.QtCore import QRegExp
from common.base import red, yell
from assembling.accounts import AssemblerConfig, BaseAccount, SignOutError, LimitQtyError, OutOfStockError, ExcludedProviderError
from common.base import blue, green, yell, red
from common.browser import Browser, WebkitError, skipUrl

log = logging.getLogger('hs')


class Config(AssemblerConfig):
    pass


class Account(BaseAccount):

    # регистрация пользователя в магазине
    def signUp(self):
        log.info('{}(): email: {}, password: {}'.format(blue('signUp'), *yell(self.accInfo.email, self.accInfo.password)))

        browser = Browser()
        post = {
            'Usr_c': self.accInfo.email[:self.accInfo.email.find('@') - 4],
            'Usr_Ps': self.accInfo.password,
            'Usr_FnaKj': self.accInfo.name,
            'Usr_LnaKj': self.accInfo.surname,
            'Usr_Add2': self.accInfo.address,
            'Usr_Add1': self.accInfo.city,
            'City_c1': '99', # без понятия, что за поле
            'Usr_Cuntry': 'Russia',
            'Usr_ZipNo': self.accInfo.postcode,
            'Usr_TelNo': self.accInfo.phone,
            'Usr_FaxNo': '',
            'Usr_HphoNo': '',
            'Usr_Mail': self.accInfo.email,
            'Usr_Mail_Kakunin': self.accInfo.email,
            'UserEnt02': 'Confirmation'
        }
        browser.post('https://www.1999.co.jp/UserEnt_e.asp', post, "$('font.txt12pt:contains(User registry is compleate)').length", pull=QRegExp('.+'))
        self.cookies = browser.cookies.dump()
        del browser


    # вход в магазин
    def signIn(self):
        log.info('{}(): email: {}, password: {}'.format(blue('signIn'), *yell(self.accInfo.email, self.accInfo.password)))

        browser = Browser()
        browser.cookies.load(self.cookies)
        browser.get('https://www.1999.co.jp/login_e.asp', "$('input.cart_button').length", pull=QRegExp('.+'))

        loginJs = '''
            $('input.xp2:eq(0)').val('%s')
            $('input.xp2:eq(1)').val('%s')
            $('input.cart_button').click()
            1
        '''

        browser.go(loginJs % (self.accInfo.email[:self.accInfo.email.find('@') - 4], self.accInfo.password), "$('div.hd_cart:contains(Log Out)').length")
        self.cookies = browser.cookies.dump()
        del browser


    # залогинены ли?
    # если нет, кидаем исключение SignOutError
    def checkSignIn(self, browser):
        log.debug('%s(): ' % blue('checkSignIn'))
        if not browser.js("$('div.hd_cart:contains(Log Out)').length"):
            raise SignOutError()


    # получение состояния корзины
    def getCartState(self):
        log.debug('%s(): ' % blue('getCartState'))

        browser = Browser()
        browser.cookies.load(self.cookies)
        browser.get('http://www.1999.co.jp/item_cart_e.asp', "$('body').length", pull=QRegExp('.+'))
        self.checkSignIn(browser)

        parseCart = '''
            var cart = {}

            $('table.txt9pt_blue:eq(0) td.cart_preorder').each(function(i) {
                var ext_id = /\d+/.exec($('a', this).attr('href'))[0]
                var qty = parseInt($(this).parent().next().find('td:eq(0)').text())
                var product = {qty: qty, backorder: false}
                cart[ext_id] = product
            })

            $('table.txt9pt_blue:eq(1) td.cart_preorder').each(function(i) {
                var ext_id = /\d+/.exec($('a', this).attr('href'))[0]
                var qty = parseInt($(this).parent().next().find('td:eq(0)').text())
                var product = {qty: qty, backorder: true}
                cart[ext_id] = product
            })
            JSON.stringify(cart)
        '''

        self.real = browser.parseToJson(parseCart)
        self.bno = browser.js("$('input[name=Bno]').val() || 0")
        self.cookies = browser.cookies.dump()
        del browser


    # добавление товара в корзину
    def addProductToCart(self, ext_id, qty):
        log.info('{}(): ext_id: {}, qty: {}'.format(blue('addProductToCart'), *yell(ext_id, qty)))

        browser = Browser()
        browser.cookies.load(self.cookies)
        browser.get('http://www.1999.co.jp/item_cart_e.asp?Cart=1&It_c=%s&piece=%s' % (ext_id, qty), "$('span:contains(The page cannot be found)').length || $('td.item_name1').length", pull=QRegExp('.+') )
        
        # проверяем, что товар на складе
        if browser.js("$('span:contains(The page cannot be found)').length"):
            raise OutOfStockError(ext_id, 0)

        self.checkSignIn(browser)

        checkLimitation = '''
            var limitation = 0
            if ($('td:contains(This item is limited)').length)
                limitation = $('td:contains(This item is limited)').text().match(/\d+/)[0]
            limitation
        '''

        limitation = browser.js(checkLimitation)
        self.cookies = browser.cookies.dump()
        del browser

        if limitation:
            raise LimitQtyError(ext_id, limitation)


    # удаление товара из корзины
    def deleteProductFromCart(self, ext_id):
        log.info('{}(): ext_id: {}'.format(blue('deleteProductFromCart'), *yell(ext_id)))

        browser = Browser()
        browser.cookies.load(self.cookies)
        post = dict()
        post['dellItem_Rev'] = ext_id
        browser.post(post, "$('body').length")
        self.cookies = browser.cookies.dump()
        del browser


    # множественная простановка количества товаров в корзине
    def quantifyProductsInCart(self, products):
        log.info('{}(): ext_id: {}'.format(blue('quantifyProductsInCart'), *yell(products)))

        browser = Browser()
        browser.cookies.load(self.cookies)

        post = dict()
        i = 0
        for ext_id in products:
            post['piece_%d' % i] = self.goal[ext_id]['qty']
            post['It_c_%d' % i] = ext_id

        browser.post('https://www.1999.co.jp/Order01_e.asp#dellclick', post, "$('body').length")
        self.cookies = browser.cookies.dump()
        del browser


    # рассчет способов доставки
    def getShippingMethods(self):
        log.info('%s(): ' % blue('getShippingMethod'))

        browser = Browser()
        browser.cookies.load(self.cookies)

        post = {
            'nCardNo01_cd': '',
            'nCardNo02_cd': '',
            'nCardNo03_cd': '',
            'nCardNo04_cd': '',
            'nLimitMM_cd': '00',
            'nTmLimitYY_cd': '00',
            'sFirstName_cd': '',
            'sLastName_cd': '',
            'Card_payment': '1',
            'security_code': '',
            'PayS': '30',
            'sTran_Cord': '930',
            'tiNoItems_Buy': 'Have',
            'tiNoItems_Rev': 'non',
            'Melmaga1': 'on',
            'AdultsItem': '0',
            'tiUri_Remark': '',
            'chkOath': '0',
            'goConfirmation': 'Go to confirmation screen',
            'Bno': self.bno,
            'Typ1_c': '101',
            'Typ2_c:': '',
            'Typ3_c': '',
            'Func': 'Buy',
            'STotal': '19080',
            'RTotal': '0',
            'name': 'ID',
            'Usr_c': self.accInfo.email[:self.accInfo.email.find('@') - 4],
            'Usr_Ps': self.accInfo.password,
            'Usr_FnaKj': 'sddsd',
            'Usr_LnaKj': 'dfsdfsd',
            'Usr_ZipNo': '111000',
            'Usr_Cuntry': 'Russia',
            'Usr_Add1': 'moskva',
            'Usr_Add2': 'dsfds wfefef',
            'Usr_TelNo': '2313243124',
            'Usr_FaxNo': '',
            'Usr_HphoNo': '',
            'Usr_Mail': self.accInfo.email,
            'PayTotal': '19080',
            'Usr_Mail_Kakunin': '',
            'Usr_c2': '',
            'Usr_Ps2': ''
        }

        browser.post('https://www.1999.co.jp/Order02_e.asp', post, "$('table.txt11pt').length")

        parseShippingMethods = '''
            var shipping = []
            var cost = $('table.txt11pt:contains(HobbySearch Shipping cost):last td:eq(3)').text().match(/\d+/)[0]
            var method = {key: 'ems', name: 'EMS', cost: cost, tracking: '1'}
            shipping.push(method)
            JSON.stringify(shipping)
        '''

        shippingMethods = browser.parseToJson(parseShippingMethods)
        self.cookies = browser.cookies.dump()
        del browser
        return shippingMethods