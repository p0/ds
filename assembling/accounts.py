import os
import psycopg2
import psycopg2.extras
import random
import string
import logging
import json
import traceback
import redis
import pickle
import time
from common.browser import Browser, WebkitError
from common.base import blue, green, yell, red, Element
from common.config import PG_USER, PG_DB, REDIS_HOST, REDIS_PORT, REDIS_DB

log = logging.getLogger('accounts')

# коннектимся к redis
log.info('connect to redis db %s on %s:%s' % yell(REDIS_DB, REDIS_HOST, REDIS_PORT))
rcon = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)


# конфигурация assembler'a
class AssemblerConfig():
    accountQty = 10
    attemptQty = 3


# базовый класс ошибок, происходящих при сборке корзины
class AccountError(Exception):
    pass


# не смогли зарегистрировать акк
class SignUpError(AccountError):
    pass


# отвалились
class SignOutError(AccountError):
    pass


# после сборки сосотояние корзины не соответсвует целевому
class StateMismatchError(AccountError):
    pass


# товар доступен в ограниченном количестве
class LimitQtyError(AccountError):
    def __init__(self, ext_id, limitation):
        self.ext_id = ext_id
        self.limitation = limitation


# необходимого количества товара нет на складе
class OutOfStockError(AccountError):
    def __init__(self, ext_id, stock):
        self.ext_id = ext_id
        self.stock = stock


# товар исключен поставщиком из продажи
class ExcludedProviderError(AccountError):
    def __init__(self, ext_id, stock):
        self.ext_id = ext_id



names = [
    'NIKOLAY', 'IVAN', 'IGOR', 'DENIS', 'EVGENIY', 'KIRILL', 'MIKHAIL', 'ANDREY', 'ILDAR', 'ALBERT',
    'INESSA', 'NATALIYA', 'MARIYA', 'EVGENIYA', 'NINA', 'IRINA', 'OKSANA', 'MARINA', 'POLINA', 'KALINA'
]

surnames = [
    'PARFINIY', 'KARIY', 'CHERNYH', 'BELYH', 'PYNZAR', 'SOBAR', 'TKACH', 'NOSIK', 'TRETYAK', 'SEDYH',
    'IVONYAK', 'JELTYAK', 'RYBAK', 'GNEDYH', 'BEGUN', 'MELNIK', 'SOLOVEY', 'DUBYAGO', 'MUHA', 'GAYDUK'
]

streets = [
    'ZHUKOVA', 'SUVOROVA', 'PUSHKINA', 'LENINA', 'MOSKOVSKAYA', 'YRALSKAYA', 'SOVETSKAYA', 'POBEDY', 'RESPUBLIKI', 'PRAVDY',
    'GAGARINA', 'GOGOLYA', 'REVOLUCII', 'TRUDA', 'MARKSA', 'ENGELSA', 'NABEREJNAYA', 'VOKZALNAYA', 'POLEVAYA', 'LUGOVAYA'
]

cities = [
    'ROSTOV', 'RYAZAN', 'NOVGOROD', 'VLADIMIR', 'AMUR', 'ASTRAHAN', 'BELGOROD', 'VORONEJ', 'IVANOV', 'KALININGRAD'
]


# если чо генерим рандомную инфу
class AccountInfo(Element):
    def __init__(self, **kwargs):
        kwargs['name'] = (kwargs.get('name') or random.choice(names)).capitalize()
        kwargs['surname'] = (kwargs.get('surname') or random.choice(surnames)).capitalize()
        kwargs['dob'] = kwargs.get('dob') or '%s-%s-%s' % (random.randint(1980, 2000), random.randint(1, 12), random.randint(1, 28))
        kwargs['email'] = kwargs.get('email') or '%s%s%s@gmail.com' % (kwargs['name'][:4].lower(), kwargs['surname'][:4].lower(), kwargs['dob'][:4])
        kwargs['password'] = kwargs.get('password') or ''.join(random.choice(string.ascii_letters + string.digits) for x in range(10))
        kwargs['phone'] = kwargs.get('phone') or '+7' + ''.join(random.choice(string.digits) for x in range(10))
        kwargs['gender'] = kwargs.get('gender') is not None or bool(random.getrandbits(1))
        kwargs['address'] = (kwargs.get('address') or '%s, %s - %s' % (random.choice(streets), random.randint(1, 50), random.randint(1, 100))).capitalize()
        kwargs['city'] = (kwargs.get('city') or random.choice(cities)).capitalize()
        kwargs['state'] = (kwargs.get('state') or kwargs['city']+ 'SKAYA').capitalize()
        kwargs['country'] = kwargs.get('country') or 'RU'
        kwargs['postcode'] = kwargs.get('postcode') or ''.join(random.choice(string.digits) for x in range(6))

        super().__init__(kwargs)


# базовый класс для реализаций
class BaseAccount:
    def __init__(self, Config, providerInfo, accInfo=None):
        self.accInfo = accInfo if accInfo else AccountInfo()
        self.Config = Config
        self.providerInfo = providerInfo
        self.real = {}
        self.goal = {}
        self.cookies = []

        self.providers = 'provider:%s' % providerInfo.id


    # регистрация пользователя в магазине
    # если не смогли зарегистрироваться должны кидать 
    def signUp(self):
        raise Exception('This method is not implemented!')


    # вход в магазин
    def signIn(self):
        raise Exception('This method is not implemented!')


    # выход из магазина
    def signOut(self):
        raise Exception('This method is not implemented!')


    # получение состояния корзины
    def getCartState(self):
        raise Exception('This method is not implemented!')


    # добавление товара в корзину
    # если не можем добавить товар, должны кидать CantAddError
    # если не можем положить указанное количество товара, должны кинуть LimitQtyError
    def addProductToCart(self, ext_id, qty):
        raise Exception('This method is not implemented!')


    # удаление товара из корзины
    def deleteProductFromCart(self, ext_id):
        raise Exception('This method is not implemented!')


    # установка количества товара в корзине
    def quantifyProductInCart(self, ext_id, qty):
        raise Exception('This method is not implemented!')


    # множественное добавление товаров в корзину
    def addProductsToCart(self, products):
        log.info('%s(): ORIGINAL' % blue('addProductsToCart'))
        for ext_id in products:
            self.addProductToCart(ext_id, self.goal[ext_id]['qty'])


    # множественное удаление товаров из корзины
    def deleteProductsFromCart(self, products):
        log.info('%s(): ORIGINAL' % blue('deleteProductsFromCart'))
        for ext_id in products:
            self.deleteProductFromCart(ext_id)


    # множественная простановка количества товаров в корзине
    def quantifyProductsInCart(self, products):
        log.info('%s(): ORIGINAL' % blue('quantifyProductsInCart'))
        for ext_id in products:
            self.quantifyProductInCart(ext_id, self.goal[ext_id]['qty'])        


    # рассчет спосбов доставки
    # должен  возвращать json со способами доставки
    # формат:
    #
    # {
    #     "key": "ключ, однозначно определяющий способ доставки у поставщика",
    #     "name": "переведенное название способа доставки",
    #     "cost": "стоимость способа доставки в валюте поставщика",
    #     "tracking": "возможность отслеживания ("1" или "0")"
    # }
    # 
    # пример:
    #
    # {
    #     "key": "ems",
    #     "name": "EMS",
    #     "cost": "2300",
    #     "tracking": "1"
    # }
    def getShippingMethods(self):
        raise Exception('This method is not implemented!')


    def sendMessage(self, type, status, data):
        messageDict = {
            'type': type,
            'status': status,
            'data': data
        }

        messageStr = json.dumps(messageDict)
        log.info('sendMessage(): %s' % yell(messageStr))
        os.write(self.pipein, bytes(messageStr, 'utf-8'))


    # сборка корзины
    # goal - целевая корзина
    # address - адрес
    # pipein - канал для передачи сообщений о состоянии сборки
    def assembleCart(self, goal, address, pipein):
        log.info('%s(): start assemble' % blue('assembleCart'))
        self.goal = goal
        self.address = address
        self.pipein = pipein
        
        # если первый раз идем собирать корзину - логинимся
        if not self.cookies:
            log.info('assembleCart(): first build')
            self.sendMessage('info', 'normal', 'Логинимся')
            self.signIn()

        attempt = 1
        while True:
            # пытаемся собрать корзину
            try:
                # получаем текущее состояние корзины
                self.sendMessage('info', 'normal', 'Получаем текущее состояние корзины')
                self.getCartState()

                # складываем в корзину недостающие товары
                missing = list(set(self.goal) - set(self.real))
                if missing:
                    self.sendMessage('info', 'normal', 'Складываем в корзину недостающие товары')
                    self.addProductsToCart(missing)

                # удаляем из корзины лишние товары
                excess = list(set(self.real) - set(self.goal))
                if excess:
                    self.sendMessage('info', 'normal', 'Удаляем из корзины лишние товары')
                    self.deleteProductsFromCart(excess)

                # не совпадает количество товаров
                disquantify = list()
                for ext_id in set(self.real) & set(self.goal):
                    if self.real[ext_id]['qty'] != self.goal[ext_id]['qty']:
                        disquantify.append(ext_id)

                # ставим верное количество товаров
                if disquantify:
                    self.sendMessage('info', 'normal', 'Ставим верное количество товаров')
                    self.quantifyProductsInCart(disquantify)

                # получаем состояние корзины после синхронизации
                if missing + excess + disquantify:
                    self.sendMessage('info', 'normal', 'Получаем состояние корзины после синхронизации')
                    self.getCartState()

                    # все товары в корзине?
                    if set(self.real) != set(self.goal):
                        raise StateMismatchError('product list mismatch!')

                    # количество указано верно?
                    for ext_id in self.real:
                        if self.real[ext_id]['qty'] != self.goal[ext_id]['qty']: 
                            raise StateMismatchError('product quantity mismatch!')

                # рассчитываем способы доставки
                self.sendMessage('info', 'normal', 'Рассчитываем способы доставки')
                shippingMethods = self.getShippingMethods()

                # переводим стоимость из валюты поставщика в рубли
                currency_course = float(rcon.hget(self.providers, 'currency_course'))
                for shippingMethod in shippingMethods:
                    shippingMethod['cost'] = round(float(shippingMethod['cost']) * currency_course)

                # отправляем клиенту
                self.sendMessage('result', 'success', json.dumps(shippingMethods))
                return

            # перелогиниваемся если отвалились
            except SignOutError:
                log.info('assembleCart(): %s' % red('sign out occured!'))
                self.sendMessage('info', 'normal', 'Логинимся')
                self.signIn()
                continue

            # ошибки браузеров при сборке
            except WebkitError:
                log.info('assembleCart(): attempt: %s, browser exceptions!' % red(attempt + 1))
                if attempt > self.Config.attemptQty:
                    self.sendMessage('result', 'error', 'При расчете способов доставки произошла ошибка на сервере')
                    return

                attempt += 1
                continue

            # состояние корзины после сборки не соответсвует требуемому
            except StateMismatchError as e:
                log.info('assembleCart(): attempt: %s, %s\n\n' % red(attempt + 1, e) + 
                    'goal:\n\n%s\n\nreal:\n\n%s\n\n' % red(json.dumps(self.goal, indent=4), json.dumps(self.real, indent=4)))

                if attempt > self.Config.attemptQty:
                    self.sendMessage('result', 'error', 'При расчете способов доставки произошла ошибка на сервере')
                    return

                attempt += 1
                continue

            # если необходимого количества товара нет на складе,
            # нет смысла дальше собирать корзину
            except OutOfStockError as e:
                log.error('assembleCart(): %s piece(s) of %s only available!' % red(e.stock, e.ext_id))
                if e.stock:
                    self.sendMessage('result', 'error', 'На складе поставщика доступно только %d единиц товара %s' % (e.stock, e.ext_id))
                else:
                    self.sendMessage('result', 'error', 'Товар %s отсутсвует на складе поставщика' % e.ext_id)

                # меняем количество товара в базе
                connection = psycopg2.connect('dbname=%s user=%s' % (PG_DB, PG_USER))
                cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
                
                sql = '''
                    UPDATE products 
                    SET stock = %s
                    WHERE id = %s
                '''

                cursor.execute(sql, [e.stock, goal[e.ext_id]['id']])
                connection.commit()
                connection.close()
                return

            # товар исключен поставщиком из продажи,
            # нет смысла дальше собирать корзину
            except ExcludedProviderError as e:
                log.error('assembleCart(): %s excluded provider!' % red(e.ext_id))
                self.sendMessage('result', 'error', 'Товар %s исключен поставщиком из продажи' % e.ext_id)

                # помечаем в товар в базе как удаленный
                connection = psycopg2.connect('dbname=%s user=%s' % (PG_DB, PG_USER))
                cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
                
                sql = '''
                    UPDATE products 
                    SET active = false
                    WHERE id = %s
                '''

                cursor.execute(sql, [goal[e.ext_id]['id']])
                connection.commit()
                connection.close()
                return   

            # если не можем добавить в корзину указанное количество товара,
            # нет смысла дальше собирать корзину
            except LimitQtyError as e:
                log.error('assembleCart(): %s limited by %s piece(s)!' % red(e.ext_id, e.limitation))
                self.sendMessage('result', 'error', 'Можно заказать только %d штук товара %s' % (e.limitation, e.ext_id))
                return

            except:
                log.info(traceback.format_exc())


# не нужно скрываться
# достаточно не привлекать внимания
class Accounts:
    def __init__(self, Account, Config, providerInfo):
        self.Config = Config                # настройки сборщика
        self.Account = Account              # реализация логики для конкретного поставщика
        self.providerInfo = providerInfo    # инфа о поставщике

        self.accounts = 'assembler:%s:accounts' % providerInfo.id
        self.carts = 'assembler:%s:carts' % providerInfo.id
        self.lock = 'assembler:%s:lock' % providerInfo.id

        # в случае неудачного завершения работы
        # мог остаться залоченным self.lock
        rcon.delete(self.lock)


    # загрузка акков из базы
    def loadAccounts(self):
        # коннектимся к базе
        log.info('loadAccounts(): connect to database: %s, user: %s' % yell(PG_DB, PG_USER))
        connection = psycopg2.connect('dbname=%s user=%s' % (PG_DB, PG_USER))
        cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # подгружаем список аккаунтов для магазина поставщика
        sql = '''
            SELECT 
                email, password, name, surname, 
                dob, phone, gender, 
                address, city, state, country
            FROM assemble_accounts 
            WHERE product_provider_id = %s
        '''
        cursor.execute(sql, [self.providerInfo.id])

        log.info('loadAccounts(): load accounts for %s( %s )' % yell(self.providerInfo.name, self.providerInfo.id))
        for data in cursor.fetchall():
            accInfo = AccountInfo(**data)
            log.info('loadAccounts(): load account email: %s' % yell(accInfo.email))
            rcon.hset(self.accounts, accInfo.email, pickle.dumps(self.Account(self.Config, self.providerInfo, accInfo)))


    # создание нового акка
    def createAccount(self):
        while True:
            # создаем акк с генерацией рандомной инфы
            log.info('createAccount(): %s' % yell("generate new account"))
            account = self.Account(self.Config, self.providerInfo)

            # пытаемся зарегать его
            try:
                account.signUp()
                log.info('createAccount(): %s' % green("sign up new account"))

                # сохранем в списке акков
                while not rcon.setnx(self.lock, 'lock'):
                    time.sleep(0.2)

                rcon.hset(self.accounts, email, pickle.dumps(account))
                rcon.delete(self.lock)

                # коннектимся к базе
                log.info('saveAccounts(): connect to database: %s, user: %s' % yell(PG_DB, PG_USER))
                connection = psycopg2.connect('dbname=%s user=%s' % (PG_DB, PG_USER))
                cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

                account.accInfo.product_provider_id = self.providerInfo.id
                
                sql = '''
                    INSERT INTO assemble_accounts (
                        email, password, name, surname, 
                        dob, phone, gender, 
                        address, city, state, country, postcode,
                        product_provider_id
                    ) VALUES (
                        %(email)s, %(password)s, %(name)s, %(surname)s, 
                        %(dob)s, %(phone)s, %(gender)s, 
                        %(address)s, %(city)s, %(state)s, %(country)s, %(postcode)s,
                        %(product_provider_id)s
                    )
                '''

                # сохраняем аккаунт в базу
                cursor.execute(sql, account.accInfo)
                connection.commit()
                connection.close()

                log.info('createAccount(): sign up account %s succesfully' % green(account.accInfo.email))
                return account

            # не смогли зарегать акк
            except SignUpError:
                log.info('createAccount(): %s' % red("can't sign up account"))


    # достаем акк
    def getAccount(self, cart_id):
        # лочим акки
        while not rcon.setnx(self.lock, 'lock'):
            time.sleep(0.2)

        # ищем акк для корзины cart_id
        email = rcon.hget(self.carts, str(cart_id))

        # если уже ходили собирать корзину из под этого акка и он не занят
        if email:
            account = rcon.hget(self.accounts, email)
            if account:
                # помечаем, что используем акк 
                rcon.hset(self.accounts, email, '')
                rcon.delete(self.lock)
                return pickle.loads(account)


        # ищем не используемый акк
        for email, account in rcon.hgetall(self.accounts).items():
            email = str(email, 'utf-8')
            log.info('getAccount(): check email: %s' % yell(email))
            if account:
                # помечаем, что используем акк 
                rcon.hset(self.accounts, email, '')
                rcon.hset(self.carts, str(cart_id), email)
                rcon.delete(self.lock)
                return pickle.loads(account)

        # все акки используются или их нет
        # пришло время создать новый акк
        log.info('getAccount(): no idle accounts')
        rcon.delete(self.lock)
        return self.createAccount()


    # обновление состояния акка
    def updateAccount(self, account):
        while not rcon.setnx(self.lock, 'lock'):
            time.sleep(0.2)

        rcon.hset(self.accounts, account.accInfo.email, pickle.dumps(account))
        rcon.delete(self.lock)
