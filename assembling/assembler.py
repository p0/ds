#!/usr/bin/env python

import os
import sys
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import json
import random
import logging
import traceback
from time import sleep
from multiprocessing import Process
from assembling.accounts import Accounts
from common.base import main, startLogging, blue, green, yell, red, checkProviderExist, getProviderInfo, makeDirs
from common.config import PROVIDERS_PATH, ASSEMBLING_PATH


# процесс-сборщик
def assemble(pipein, data, accounts):
    @main
    def mainloop():
        request = json.loads(data)

        # формат запроса к сборщику:
        # 
        # {
        #     'cart_id': ..., 
        #     'address': { 'country_code': ... }, 
        #     'content': { 'product_ext_id': {'product_id': ..., 'qty': ..., ... }, ... }
        # }
        #
        log.info('assemble(): request\n\n%s\n' % green(json.dumps(request, indent=2, separators=(',', ': '))))

        # выбираем акк
        account = accounts.getAccount(request['cart_id'])

        # собираем корзину
        account.assembleCart(request['content'], request['address'], pipein)

        # сохраняем изменения
        accounts.updateAccount(account)

    mainloop()


# управляющий процесс
if __name__ == '__main__':
    # проверяем, что переданы все параметры
    if len(sys.argv) < 2:
        print('Error: expected provider name!\nUsage : %s <provider name>' % sys.argv[0])
        sys.exit(1)

    # проверяем, что поставщик существует
    providerName = sys.argv[1]
    if not checkProviderExist(providerName):
        sys.exit(1)

    # создаем  необходимые каталоги, если их нету
    logdir = os.path.join(ASSEMBLING_PATH, 'logs')
    lstdir = '/tmp/assembler_listeners/'
    makeDirs(logdir, lstdir)

    # начинаем логгирование
    startLogging(os.path.join(logdir, providerName))
    log = logging.getLogger('mngproc')
    log.info(green('*** ASSEMBLER IS RUNNING ***'))

    # импортим настройки и реализацию аккаунта для магазина
    exec('from providers.%s.accounts import Config, Account' % providerName)

    # инфа о поставщике
    providerInfo = getProviderInfo(providerName)

    # грузим из базы список аккаунтов
    accounts = Accounts(Account, Config, providerInfo)
    accounts.loadAccounts()

    loop = tornado.ioloop.IOLoop.instance()

    class LoadShippingMethodsHandler(tornado.websocket.WebSocketHandler):
        def open(self):
            log.info('open(): new connection')
            self.write_message('{"message": "new connection"}')


        @tornado.web.asynchronous
        def on_message(self, message):
            pipeout, pipein = os.pipe()
            log.info('on_message(): received message %s, fd: %s' % yell(message, pipeout))
            process = Process(target=assemble, args=(pipein, message, accounts))
            process.daemon = True
            process.start()
            loop.add_handler(pipeout, self.async_callback(self.on_respond), loop.READ)


        def on_respond(self, fd, events):
            log.info('on_respond(): fd: %s' % yell(fd))

            # вычитываем сообщение
            # TODO: переписать
            message = str(os.read(fd, 4096), 'utf-8')
            self.write_message(message)

            # если расчитали способы доставки или случилась ошибка, перестаем следить за каналом
            if json.loads(message)['type'] == 'result':
                log.info('on_respond(): remove handler %s' % yell(fd))
                loop.remove_handler(fd)


        def on_close(self):
            log.info('on_close(): connection closed')



    try:
        application = tornado.web.Application([('/load_shipping_methods', LoadShippingMethodsHandler)])
        http_server = tornado.httpserver.HTTPServer(application)
        http_server.listen(8888)
        loop.start()

    # ловим ^C
    except KeyboardInterrupt:
        log.info(green('*** ASSEMBLER STOPPED ***'))
        sys.exit()


