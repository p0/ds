import os
import sys
import signal
import traceback
import logging
from PyQt4.QtCore import QTimer
from PyQt4.QtGui import QApplication, qApp
import psycopg2
import psycopg2.extras
from common.config import PG_USER, PG_DB, PROJECT_PATH, PROVIDERS_PATH

BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = (i + 30 for i in range(8))
RESET_SEQ = '\x1b[0m'
COLOR_SEQ = '\x1b[1;%dm'


def startLogging(logname='test'):
    format = '%(asctime)s.%(msecs)03d  %(process)d %(name)-12s %(levelname)-8s %(message)s'
    datefmt = '%d-%m-%y %H:%M:%S'
    level = logging.DEBUG
    logging.basicConfig(filename=logname + '.log', format=format, datefmt=datefmt, level=level)
    '''
    format = '%(asctime)s.%(msecs)d  %(process)d %(name)-12s %(levelname)-8s %(message)s'
    datefmt = '%d-%m-%y %H:%M:%S'
    level = logging.DEBUG

    handler = logging.handlers.RotatingFileHandler(logname)
    formatter = logging.Formatter(format, datefmt=datefmt)
    handler.setFormatter(formatter)
    handler.setLevel(level)
    logger = logging.getLogger()
    logger.addHandler(handler)
    '''


def cutString(string, length=15):
    string = str(string)
    return string if len(string) < length else string[:length] + '...'


def colorize(color, *args):
    if len(args) == 1:
        if not isinstance(args[0], (list, tuple)):
            return COLOR_SEQ % color + str(args[0]) + RESET_SEQ
        else:
            args = args[0]
    return tuple(map(lambda arg: COLOR_SEQ % color + str(arg) + RESET_SEQ, args))


def red(*args):
    return colorize(RED, *args)


def green(*args):
    return colorize(GREEN, *args)


def yell(*args):
    return colorize(YELLOW, *args)


def blue(*args):
    return colorize(BLUE, *args)


def mag(*args):
    return colorize(MAGENTA, *args)


# def newnym():
#     print 'NEWNYM'
#     conn = TorCtl.connect()
#     conn.send_signal('NEWNYM')

# декоратор
def main(func):
    def wrapper():
        try:
            func()

        # ловим ^c
        except KeyboardInterrupt:
            print('KeyboardInterrupt')
        except:
            print(traceback.format_exc())
        finally:
            # QApplication.quit()
            # qApp.closeAllWindows()
            # TODO: вероятно баг в qt, стоит порыть эту тему, а пока что дешево и сердито
            sys.exit()

    app = QApplication([])
    # имитация события onStartApplication
    QTimer.singleShot(0, wrapper)
    app.exec_()



class ElementAttributeError(Exception):
    def __init__(self, attribute, element):
        super().__init__("Attribute '%s'" % attribute, element)



# c объектами класса Element можно работать как с объектами в javasript'e
class Element(dict):
    def __getitem__(self, attr):
        if not attr in self:
            raise ElementAttributeError(attr, self)
        return dict.__getitem__(self, attr)


    def __delitem__(self, attr):
        if not attr in self:
            raise ElementAttributeError(attr, self)
        dict.__delitem__(self, attr)


    def __getattr__(self, attr):
        if attr[:2] == '__':
            return dict.__getattr__(self, attr)
        return self[attr]


    def __setattr__(self, attr, value):
        self[attr] = value

        
    def __delattr__(self, attr):
        del self[attr]


# проверяем, что поставщик существует
def checkProviderExist(providerName):
    providers = next(os.walk(PROVIDERS_PATH))[1]
    if providerName not in providers:
        print('Error: unexist provider %s\nThere are only: %s' % (providerName, ', '.join(providers)))
        return False
    return True


def getProviderInfo(providerName):
    pgcon = psycopg2.connect('dbname=%s user=%s' % (PG_DB, PG_USER))
    cursor = pgcon.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql = 'SELECT id, name, currency FROM product_providers WHERE name = %s'
    cursor.execute(sql, [providerName])
    providerInfo = Element(cursor.fetchone())
    pgcon.close()
    return providerInfo


def makeDirs(*dirs):
    for dir in dirs:
        if not os.path.exists(dir):
            os.makedirs(dir)


if __name__ == '__main__':
    print(red('ololo'))
    print('%s %s %s %s' % yellow(2, 2e-1, True, 'lalala'))
