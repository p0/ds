#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import logging
import traceback
from datetime import datetime
from email.utils import formatdate
from PyQt4.QtCore import QEventLoop, QFileInfo, QFile, QFileInfo, QUrl
from PyQt4.QtCore import QRegExp, QIODevice, QTimer, QByteArray, QVariant, QDateTime
from PyQt4.QtGui import QImage
from PyQt4.QtWebKit import QWebPage, QWebView, QWebSettings
from PyQt4.QtNetwork import QNetworkAccessManager, QNetworkDiskCache, QNetworkRequest, QNetworkProxy
from PyQt4.QtNetwork import QNetworkCookie, QNetworkCookieJar
from lxml import etree, objectify
from common.config import JQUERY_PATH
from common.base import main, startLogging, red, yell, green, mag


log = logging.getLogger('browser')

# ошибки webkit'a
class WebkitError(Exception):
    pass

# слишком долго ждем загрузки
class TimeoutError(WebkitError):
    pass

# пришла не та страница
class VerifyError(WebkitError):
    pass

# ошибка сети
class NetworkError(WebkitError):
    pass

# ошибка в ответе
class ReplyError(WebkitError):
    pass


# заглушка для ненужных запросов
skipUrl = QUrl('skip://localhost/')
# названия http операций
ops = ('UNKNOWN', 'HEAD', 'GET', 'PUT', 'POST', 'DELETE', 'SKIP')

# Ответ сервера: данные, заголовки и аттрибуты
class Reply:
    def __init__(self):
        self.content = QByteArray()
        self.headers = None
        self.attribs = None


# Стандартно QNetworkAccessManager не имеет нормального интерфейса для доступа к загруженным данным
# Поэтому пишем свой фиксенный менеджер, который умеет:
#   сохранять страницы, картинки и скрипты после загрузки
#   загружать только с указанных адресов
# В режиме сохранения будем копировать все ответы в replies, прежде чем их прочитает браузер
# replies - словарь {url: Reply}
class Manager(QNetworkAccessManager):

    # После полной загрузки данных, копируем аттрибуты и заголовки в replies
    def onFinished(self):
        reply = self.sender()

        attribs = dict()
        attribs[QNetworkRequest.HttpStatusCodeAttribute] = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)

        headers = dict()
        for name in reply.rawHeaderList():
            headers[str(name, 'utf-8')] = str(reply.rawHeader(name), 'utf-8')

        url = reply.url().toString()
        self.replies[url].headers = headers
        self.replies[url].attribs = attribs

        # покажем для каких адресов есть сохраненный контент
        log.debug('onFinished(): available content for: %s' % yell(', '.join(self.replies)))


    # По кусочкам копируем загруженные данные в replies
    # Не можем сделать это readAll'ом в onFinished, т.к. браузер к тому моменту уже вычитывает данные из reply
    # C unsupported content, можно было бы сделать readAll, т.к. браузер не вычитывает данные из такого reply
    def onReadyRead(self):
        reply = self.sender()

        if self.loadingUnsupportedContent:
            log.debug('onReadyRead(): load %s bytes of unsupported content' % yell(reply.bytesAvailable()))
            self.replies[reply.url().toString()].content.append(reply.read(reply.bytesAvailable()))
        else:
            log.debug('onReadyRead(): load %s bytes' % yell(reply.bytesAvailable()))
            self.replies[reply.url().toString()].content.append(reply.peek(reply.bytesAvailable()))

    
    # корректируем запрос в соответствии с нашими пожеланиями
    def createRequest(self, op, request, device=None):
        # выставляем наши хидеры
        for header, value in self.headers.items():
            request.setRawHeader(header, value)
        '''
        request.setRawHeader('User-Agent', 'bebe')
        #req.setRawHeader( "Accept-Language", "en-us,en;q=0.5" )
        if self.debug:
            for header in request.rawHeaderList():
                print '%s: %s' %(header, request.rawHeader(header))

            print type(request.originatingObject())
            print
        '''

        # на какой url хотим пойти?
        url = request.url().toString()

        # получаем название операции
        operation = ops[op]

        # а он в списке для загрузки?
        for re in self.pull:
            if re.exactMatch(url):
                break
        # если нет, то подменим url на пустой
        else:
            request.setUrl(skipUrl)
            operation = ops[6]

        log.debug('createRequest(): %s %s' % yell(operation, url))

        # создаем запрос
        reply = QNetworkAccessManager.createRequest(self, op, request, device)

        # при сохранении подключаем слоты
        if self.saveMode:
            self.replies[request.url().toString()] = Reply()
            reply.readyRead.connect(self.onReadyRead)
            reply.finished.connect(self.onFinished)

        # сбрасываем флаг перед началом закачки
        self.loadingUnsupportedContent = False

        return reply


# Кука в виде питонообъекта
class Cookie():
    def __init__(self, name, value, domain, path, expirationDate, httpOnly, secure):
        self.name = name
        self.value = value
        self.domain = domain
        self.path = path
        self.expirationDate = expirationDate
        self.httpOnly = httpOnly
        self.secure = secure


    def __str__(self):
        s = "%s: '%s', %s%s, %s" % (self.name, self.value, self.domain, self.path, self.expirationDate)
        if self.httpOnly:
            s += ', HttpOnly'
        if self.secure:
            s += ', Secure'
        return s


# Человеческий интерфейс к кукам
class Cookies():
    def __init__(self, cookieJar):
        self.cookieJar = cookieJar


    # получение куки по имени
    def get(self, name):
        log.info('get(): name: %s' % yell(name))
        for cookie in self.cookieJar.allCookies():
            if cookie.name() == name:
                return cookie
        return None


    # добавление новой куки для указанного url'a
    def set(self, name, value, url):
        log.info('set(): name: %s, value: %s, url: %s' % yell(name, value, url))
        if not isinstance( url, QUrl ):
            url = QUrl( url )
        self.cookieJar.setCookiesFromUrl([QNetworkCookie(name, value)], url)


    # дампим куки как питоновские объеты, 
    # потому что не можем использовать объекты qt
    def dump(self):
        log.info('dump(): ')
        cookies = []
        for c in self.cookieJar.allCookies():
            cookies.append(Cookie(c.name(), c.value(), c.domain(), c.path(), str(c.expirationDate), c.isHttpOnly(), c.isSecure()))
        return cookies


    def load(self, cookies):
        log.info('load():')
        qtCookies = []
        for cookie in cookies:
            qtCookie = QNetworkCookie(cookie.name, cookie.value)
            qtCookie.setDomain(cookie.domain)
            qtCookie.setPath(cookie.path)
            qtCookie.setExpirationDate(QDateTime.fromString(cookie.expirationDate))
            qtCookie.setHttpOnly(cookie.httpOnly)
            qtCookie.setSecure(cookie.secure)
            qtCookies.append(qtCookie)

        self.cookieJar.setAllCookies(qtCookies)

    
    def __str__( self ):
        return ', '.join([str(c.toRawForm()) for c in self.cookieJar.allCookies()])

    

# QWebPage работает асинхронно, нам требуется работать синхронно
# Для этого создаем ожидалку, которая ждет либо наступления некоторого события, либо таймаута
class Waiter():
    def __init__(self, exitSignal, restartSignal, timeoutMs):
        self.exitSignal = exitSignal
        self.restartSignal = restartSignal

        self.timeoutMs = timeoutMs
        self.idleMs = 3000
        self.timeoutTimer = QTimer()
        self.timeoutTimer.setSingleShot(True)
        self.timeoutTimer.timeout.connect(self.onTimeout)

        self.idleTimer = QTimer()
        self.idleTimer.timeout.connect(self.onIdle)
        self.loop = QEventLoop()


    # дождались нужного нам сигнала
    # отцепляем остальные сигналы
    # выходим из цикла ожидания
    def onExitSignal(self):
        self.restartSignal.disconnect(self.onRestartSignal)
        self.timeoutTimer.timeout.disconnect(self.onTimeout)
        self.idleTimer.timeout.disconnect(self.onIdle)
        self.waited = True
        self.loop.quit()
        log.debug('onExitSignal(): %s' % mag('finished'))


    # рестартим таймер
    def onRestartSignal(self):
        self.timeoutTimer.start(self.timeoutMs)
        self.idleTimer.start(self.idleMs)
        log.debug('onRestartSignal(): %s' % mag('restart timer'))


    # не дождались
    # выходим из цикла ожидания
    def onTimeout(self):
        self.exitSignal.disconnect(self.onExitSignal)
        self.restartSignal.disconnect(self.onRestartSignal)
        self.idleTimer.timeout.disconnect(self.onIdle)
        self.waited = False
        self.loop.quit()
        log.debug('onTimeout(): %s' % mag('finished'))


    # простаиваем
    def onIdle(self):
        log.debug('onIdle(): %s' % mag('Zzzzz...'))


    # метод, в котором непосредственно реализовано ожидание
    # exitSignal - ожидаемый сигнал
    # restartSignal - сигнал рестартнуть таймер
    # timeoutMs - таймаут
    # возвращает дождались/не дождались
    def wait(self):
        self.exitSignal.connect(self.onExitSignal)
        self.restartSignal.connect(self.onRestartSignal)
        self.timeoutTimer.start(self.timeoutMs)
        self.idleTimer.start(self.idleMs)
        log.debug('wait(): %s' % mag('started'))
        self.loop.exec_()
        return self.waited


# Браузер, который умеет ходить на указанные url'ы и выполнять там разные js
# autoJquerify - автоматическая подгрузка JQuery к загруженной странице
# loadImages - загрузка изображений со страницы
# timeoutMs - т.к ходим через медленные прокси, позволяем страницам грузится подольше
class Browser(QWebPage):

    def __init__(self, autoJquerify=True, loadImages=False, timeoutMs=60000):
        super().__init__()
        self.autoJquerify = autoJquerify
        self.timeoutMs = timeoutMs

        # грузим картинки?
        QWebSettings.globalSettings().setAttribute(QWebSettings.AutoLoadImages, loadImages)
        # отключаем плагины
        QWebSettings.globalSettings().setAttribute(QWebSettings.PluginsEnabled, False)
        # качаем unsupported content
        self.setForwardUnsupportedContent(True)

        self.loadStarted.connect(self.onLoadStarted)
        self.loadFinished.connect(self.onLoadFinished)
        self.unsupportedContent.connect(self.onUnsupportedContent)

        # фиксенный менеджер
        self.manager = Manager()
        self.setNetworkAccessManager(self.manager)

        # человеческий доступ к кукам
        self.manager.setCookieJar(QNetworkCookieJar())
        self.cookies = Cookies(self.manager.cookieJar())

        # загружаем jQuery из файла
        log.debug('init(): load from %s' % yell(JQUERY_PATH))
        self.jquery = open(JQUERY_PATH).read()

        log.info('init(): autoJquerify: %s, timeoutMs: %s' % yell(autoJquerify, timeoutMs))


    # обработчик старта загрузки страницы
    def onLoadStarted(self):
        # если сохраняем данные, удалим старье
        if self.manager.saveMode:
            self.manager.replies = dict()
            

    # обработчик завершения загрузки страницы
    def onLoadFinished(self, ok):
        pass
        # if not ok:
        #     raise NetworkError('Network problem')


    # обработчик 
    def onUnsupportedContent(self, reply):
        # говорим manager'y, что качаем unsupported content
        self.manager.loadingUnsupportedContent = True



    # логгирование сообщений об ошибках при выполнении js
    #def javaScriptConsoleMessage(self, msg, line, source):
        #log.warning(colorize('jsconsole(): %s line %d: %s' % (source, line, msg), RED))


    # засыпание на ms секунд
    def sleep(self, ms):
        log.info('sleep(): started')
        loop = QEventLoop()
        QTimer.singleShot(ms, loop.quit)
        loop.exec_()
        del loop
        log.info('sleep(): finished')


    # выполнение javascript
    def js(self, script, printExpr=True, printRes=True):
        if printExpr:
            log.debug('js(): evalute %s' % yell(script))

        result = self.mainFrame().evaluateJavaScript(script)

        # js должен возвращать строку или число
        if result is None:
            raise Exception('Invalid value. Checkout javascript exceptions above!')

        if printRes:
            log.debug('js(): result %s' % green(result))

        return result


    # выполняет js, который возвращает xml в виде строки
    # мы парсим этот xml и возвращаем objectify
    def parseToXml(self, script):
        result = objectify.fromstring(self.js(script, printRes=False))
        log.debug('parseToXml(): result\n\n%s' % green(etree.tostring(result, pretty_print=True)))
        return result


    # выполняет js, который возвращает json в виде строки
    # мы парсим этот json и возвращаем питоновский список или словарь
    def parseToJson(self, script):
        result = json.loads(self.js(script, printRes=False))
        json.dumps(result, indent=2, separators=(',', ': '))
        log.debug('parseToJson(): result\n\n%s' % green(json.dumps(result, indent=2, separators=(',', ': '))))
        return result


    # выполнение js, которое влечет за собой загрузку данных (страницы, картинки и т.д)
    # verifyJs - js для проверки страницы
    # headers - правильные хидеры (например User-agent, Referrer), старые при этом затираются
    # pull - что выкачивать
    # saveMode - сохранять данные
    def go(self, script, verifyJs, headers=dict(), pull=QRegExp('.+'), saveMode=False):
        self.manager.headers = headers
        if not isinstance(pull, (list, tuple)):
            self.manager.pull = [pull]
        self.manager.saveMode = saveMode

        log.debug('go(): js processing')

        # выполняем js
        self.js(script)

        # началась загрузка, ждем окончания
        if Waiter(self.loadFinished, self.loadProgress, self.timeoutMs).wait():
            log.debug('go(): loading finished')
        else:
            #self.action(QWebPage.Stop)
            raise TimeoutError('Loading finished timeout!')


        # подгрузим jQuery, если надо
        if self.autoJquerify:
            self.mainFrame().evaluateJavaScript(self.jquery)

        # убедимся что пришла ожидаемая страница
        log.debug('go(): verify page')
        if not self.js(verifyJs, printRes=False):
            # если валидатор не прошел дампим страницу
            self.dump()
            raise VerifyError('Wrong page!')
        log.debug('go(): successfull verify')


    # GET запрос на указанный url
    # attempts - количество попыток загрузки
    # delaySec - задержка между попытками
    # остальные параметры см. Browser.go()
    def get(self, url, verifyJs, headers=dict(), pull=None, saveMode=False, attempts=1, delaySec=1):
        log.info('get(): url: %s, verifyJs: %s, headers: %s, pull: %s' % yell(url, verifyJs, headers, pull))
        if pull == None:
            pull = QRegExp(url, syntax=QRegExp.FixedString)

        # пытаемся зайти на url указанное количество раз
        for i in range(attempts):
            try:
                self.go('window.location = "%s"' % url, verifyJs, headers, pull, saveMode)
                return
            except WebkitError:
                log.error('get(): %s attempt(s) is failed' % red(i + 1))

                # если не смогли, выбрасываем последнее исключение
                if i == attempts - 1:
                    raise

                # спим перед следующей попыткой
                self.sleep(delaySec)


    # POST запрос на указанный url
    # post - {key1: value1, key2: value2,.. }
    # остальные параметры см. Browser.get()
    def post(self, url, post, verifyJs, headers=dict(), pull=None, saveMode=False, attempts=1, delaySec=1):
        log.info('post(): url: %s, post: %s, verifyJs: %s, headers: %s, pull: %s' % yell(url, post, verifyJs, headers, pull))
        if pull == None:
            pull = QRegExp(url, syntax=QRegExp.FixedString)

        # делаем форму, чтобы потом ее постануть
        js = 'var form = document.createElement("form");'
        js += 'form.setAttribute("method", "post");'
        js += 'form.setAttribute("action", "%s");' % url
        for key, val in post.items():
            js += 'var hiddenField = document.createElement("input");'
            js += 'hiddenField.setAttribute("type", "hidden");'
            js += 'hiddenField.setAttribute("name", "%s");' % key
            js += 'hiddenField.setAttribute("value", "%s");' % val
            js += 'form.appendChild(hiddenField);'

        js += 'document.body.appendChild(form);'
        js += 'form.submit(); 1;'


        # пытаемся постануть данные указанное количество раз
        for i in range(attempts):
            try:
                self.go(js, verifyJs, headers, pull, saveMode)
                break
            except WebkitError as e:
                log.error('get(): %s attempt(s) is failed' % red(i + 1))

                # если не смогли, выбрасываем последнее исключение
                if i == attempts - 1:
                    raise

                # спим перед следующей попыткой
                self.sleep(delaySec)


    # сохранение страницы
    def dump(self):
        dumpdir = 'dumps'

        # создаем каталог для дампов, если нету
        if not os.path.exists(dumpdir):
            os.makedirs(dumpdir)

        # сохраняем главный фрейм
        filename = os.path.join(dumpdir, datetime.now().isoformat() + '.html')
        log.info('dump(): filename: %s' % yell(filename))
        f = open(filename, 'w')
        f.write(self.mainFrame().toHtml())
        f.close()

    
    # сохранение загруженных данных (например картинок)
    # данные берутся из кеша по url'у  и сохраняются в path
    def save(self, url, path):
        log.info('save(): %s to %s' % green(url, path))
        file = QFile(path)
        if file.open(QIODevice.WriteOnly):
            file.write(self.manager.replies[url].content)
        file.close()


    # возвращает заголовки последнего reply
    def headers(self, url):
        heads = []
        for k, v in self.manager.replies[url].headers.items():
            heads.append(yell('%s: %s' % (k, v)))
        log.info('headers(): %s' % ', '.join(heads))
        return self.manager.replies[url].headers


    # возвращает аттрибуты последнего reply
    def attribs(self, url):
        log.debug(str(self.manager.replies[url].attribs))


    # загрузка файла с последующим сохранением в path/url.path()
    def load(self, url, path):
        log.info('load(): url: %s, path: %s' % yell(url, path))

        # собираем путь к файлу
        path = os.path.join(path, QUrl(url).path()[1:])

        # получаем дату последней модификации файла, если он существует, или 1 января 1970
        modifiedSince = formatdate(os.path.getmtime(path) if os.path.exists(path) else 0)[:-5] + 'GMT'

        # загружаем файл, если он изменился с прошлого раза
        self.get(url, '1', headers=dict({'If-Modified-Since': modifiedSince}), pull=QRegExp('.+'), saveMode=True)

        # берем reply по url
        reply = self.manager.replies.get(url)

        # куда это он делся?
        if not reply:
            raise ReplyError("No reply for '%s'!" % url)

        status = reply.attribs[0]
        declSize = int(reply.headers.get('Content-Length', 0))
        realSize = reply.content.size()

        # если файл не изменился с прошлой загрузки
        if status == 304:
            log.info('load(): file was not modified from %s' % yell(modifiedSince))
            return

        # если нам не отдали файл или его удалили, или еще что-нибудь произошло
        if status != 200:
            # log.error('load(): HTTP status: %s' % red(status))
            # return
            raise ReplyError('HTTP status: %s!' % status)

        log.debug('load(): check size. declare: %s, real: %s' % yell(declSize, realSize))
        # если пришло 5 литров жидкого вакуума или размер файла не совпадает с заявленным, выкинем исключение
        if declSize == 0 or realSize != declSize:
            raise ReplyError('Incorrect file size!')

        # пробуем создать каталоги, указанные в пути
        dname = os.path.dirname(path)
        try:
            os.makedirs(dname)
            log.info('load(): create dir %s' % yell(dname))
        except:
            log.warning("load(): can't create dir %s" % yell(dname))
        self.save(url, path)


    # показ отладочного окна
    def show(self):
        self.view = QWebView()
        self.view.setPage(self)
        self.view.show()


if __name__ == '__main__':

    # пример использования класса Browser
    @main
    def example():
        startLogging()

        # создаем Browser
        browser = Browser()

        # включаем визуальный режим
        # используется при отладке
        browser.show()

        # идем на http://www.amiami.com/
        browser.get('http://www.amiami.com/', "$('body').length")

        # а с таким валидатором не пройдет
        # browser.get('http://www.amiami.com/', "0")

        # хотим узнать название первого товара из новых поступлений
        print(browser.js("$('table.product_table li.product_name:eq(1)').text()"))

        # распарсим дерево категорий
        # для этого напишем js, который соберет корневые категории
        parseCategory = '''
            var getExtid = /.+\/(.+)\.html.*/

            var root = $('<root><provider /></root>')
            var provider = $('provider', root)

            $('.genre_list:gt(0)').each(function(i) {
                var category = $('<category/>')
                provider.append(category)

                var extid = getExtid.exec($('a', this).attr('href'))[1]
                var name = $(this).text()

                category.attr('extid', extid)
                category.attr('name', name)
            })

            root.html()
        '''

        # и вызовем метод parse, который вернет нам objectify
        parsedCategoryXml = browser.parseToXml(parseCategory)
        # покажем xml
        print(etree.tostring(parsedCategoryXml, pretty_print=True))


        # то же самое, только с json'ом
        parseCategory = '''
            var getExtid = /.+\/(.+)\.html.*/

            var root = []
            $('.genre_list:gt(0)').each(function(i) {
                var category = {type: "category", parent: "root"}
                root.push(category)

                var extid = getExtid.exec($('a', this).attr('href'))[1]
                var name = $(this).text()

                category.extid = extid
                category.name = name
            })

            JSON.stringify(root)
        '''

        parsedCategoryJson = browser.parseToJson(parseCategory)
        print(json.dumps(parsedCategoryJson, indent=2, separators=(',', ': ')))


        # хотим узнать адрес изображения к этому товару
        src = browser.js("$('table.product_table div.product_img:eq(1) img').attr('src')")
        print(src)

        # хотим скачать это изображение
        # для этого выставляем saveReplies=True
        browser.get(src, '1', saveMode=True)

        # изображение скачалось и теперь находится в браузере
        browser.save(src, 'ololo.jpeg')

        # то же самое, только проще
        browser.load(src)


    # проверяем, как работают примеры
    example()