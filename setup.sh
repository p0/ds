#!/bin/bash

# установка виртуального окружения для проекта
install_virtualenv() {
    echo 'Install virtualenv'

    # переходим в корень проекта
    cd $(dirname $0)
    DS_DIR=$PWD

    # если уже есть виртуальное окружение, удалим его
    if [ -d env ]; then
        rm -R env
    fi

    # создаем виртуальное окружение для проекта
    # будем использовать системные пакеты
    # используем в проекте python 3, ибо замучился с кодировками
    virtualenv --system-site-packages -p $(which python3) env
    
    # экспортируем DS_DIR
    echo "export DS_DIR=$DS_DIR" | tee -a env/bin/activate

    # говорим питону искать пакеты в нашем проекте
    echo "$DS_DIR" > env/lib/python3.2/site-packages/ds.pth
}


# установка пакетов
install_apt_packs() {
    echo 'Install apt packages'

    # список пакетов для установки
    packs=( \
        mc \
        vim \
        postgresql \
        xvfb \
        libjpeg8-dev \
        redis-server \
        python3 \
        python3-psycopg2 \
        python3-pyqt4 \
        python3-lxml \
        python-virtualenv \
        python3-all-dev \
        python3-tornado \
    )

    # ставим
    sudo apt-get install ${packs[@]}
}


install_pip_packs() {
    echo 'Install pip packages'

    # список пакетов для установки
    packs=( \
        Pillow \
        redis \
    )

    # ставим
    pip install ${packs[@]}
}


# генерация 
gen_config_common() {
    CONF="$DS_DIR/common/config.py"
    echo "Generation $CONF"
    sed "s%{PROJECT_PATH}%$DS_DIR/%g" $DS_DIR/common/config.template | \
    sed "s%{PG_HOST}%localhost%g" | \
    sed "s/{PG_PORT}/5432/g" | \
    sed "s/{PG_USER}/ds/g" | \
    sed "s/{PG_DB}/ds/g" | \
    sed "s/{REDIS_HOST}/localhost/g" | \
    sed "s/{REDIS_PORT}/6379/g" | \
    sed "s/{REDIS_DB}/0/g" > $CONF
}


# генерация конфигов
gen_configs() {
    echo 'Generation configs'
    gen_config_common
}


# генерация расписания
get_crontab() {
    CRONTAB="$DS_DIR/setup/crontab"
    echo "Generation $CRONTAB"
    sed "s%{PROJECT_PATH}%$DS_DIR/%g" $DS_DIR/setup/crontab.template > $CRONTAB
}


# установка расписания
set_shedule() {
    echo 'Setup shedule for cron'
    get_crontab
    crontab "$DS_DIR/setup/crontab"
}


install_apt_packs
install_virtualenv
install_pip_packs
gen_configs
set_shedule