#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from os import path 
from common.base import main, startLogging, blue, green, yell, red
from common.config import PROJECT_PATH
from common.browser import Browser
from utils.sendsms import sendSms

startLogging('ipchecker')
log = logging.getLogger('checker')

@main
def checkIp():
    log.info('checkIp(): %s' % green('checking ip is started'))

    # получаем на каком-то левом сайте наш ip
    browser = Browser()
    browser.get('http://www.myip.ru/en-EN/index.php', "$('tr:contains(Your IP address)').length")
    currentIp = browser.js("$('tr:contains(Your IP address):last').next().text()")
    log.info('checkIp(): current ip %s' % yell(currentIp))

    fname = path.join(path.abspath(path.dirname(__file__)), 'ip.dat')

    # пытаемся прочитать ip при прошлой проверке
    try:
        f = open(fname, 'r')
        savingIp = f.read()
        f.close()
    except IOError:
        savingIp = ''
    log.info('checkIp(): saving ip %s' % yell(savingIp))

    # если сменился, запишем в файл и отправим sms
    if currentIp != savingIp:
        log.info('checkIp(): %s' % blue('ip was changed'))
        f = open(fname, 'w')
        f.write(currentIp)
        f.close()
        sendSms('polzuka', 'vol70CM3', currentIp, 'IP was changed', 'Raspberry Pi')
    
    log.info('checkIp(): %s' % green('checking ip is finished'))


if __name__ == '__main__':
    checkIp()
    